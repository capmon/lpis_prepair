#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging

import os
import sys
import glob
import shutil
import subprocess
import itertools
import copy
import fiona
from shapely.geometry import shape, box, Point, mapping, MultiPolygon
from shapely.ops import cascaded_union, unary_union

from rtree import index
from tqdm import tqdm
import networkx as nx
import sqlite3
from sqlite3 import Error
import pandas as pd

exclude_crop_code9 = ["BORDURE", "SURF_TNEX", "BOIS_PATU"]

test_fid = ["0681580430008008", "0680106620009009"]


def length_at_distance(geom1, geom2, distance):
    """
    compute the lenght of geom2 border with is inside a distance of geom1

    :param geom1:
    :param geom2:
    :param distance:
    :return:
    """
    clip_geom = geom1.buffer(distance)
    if not geom2.geom_type in ["Polygon", "MultiPolygon"]:
        raise ValueError(f"geom2 must be a Polygon or MultiPolygon not {geom2.geom_type}")
    border2 = geom2.boundary
    common_border2 = border2.intersection(clip_geom)
    return common_border2.length


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="process foi from a lpis shapefile")

    parser.add_argument("-i", "--in_lpis", dest="in_shp", help="shapefile", required=True)
    parser.add_argument("-w", "--work_dir", dest="work_dir", help="working directory for temporary files")
    parser.add_argument("-th", "--threshold_ha", type=float, dest="threshold_ha",
                        help="threshold in hectare for small parcell selection")

    parser.add_argument("-f", "--field_code", dest="field_code",
                        help="name of the field with different crop code", required=True)
    parser.add_argument("-uid", "--field_id", dest="field_id",
                        help="name of the field with parcel unique identifier", required=True)
    parser.add_argument("-e", "--exclude_codes", dest="exclude_codes", nargs='+',
                        help="list of code to exclude for merging", required=False)
    parser.add_argument("-p", "--out_prefix", dest="out_prefix",
                        help="prefix to use for output file, default to input shp name", required=False)
    parser.add_argument("-vi", "--verbose_id", dest="verbose_id", nargs='+',
                        help="id where print verbose messages", required=False)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    crop_fieldname = args.field_code
    uid_fieldname = args.field_id

    if args.threshold_ha:
        surf_min_m2 = args.threshold_ha * 10000
    else:
        surf_min_m2 = 5000

    if args.work_dir:
        work_dir = os.path.abspath(args.work_dir)
    else:
        work_dir = os.path.abspath(".")

    if args.exclude_codes:
        exclude_crop_code = args.exclude_codes
    else:
        exclude_crop_code = ["BFS", "BFP", "BTA", "BOR", "SNE", "BOP"]

    no_small_parcels_count = 0
    except_parcels_count = 0
    small_parcels = []

    if args.out_prefix:
        basename = args.out_prefix
    else:
        basename = os.path.splitext(os.path.basename(args.in_shp))[0]

    if args.verbose_id:
        verbose_fid = args.verbose_id
    else:
        verbose_fid = []

    foi_cat_prop = 'foi_cat'
    # step 1 load all data in memory and in a list of feature (geojson)
    print(f" load lpis feature")
    with fiona.open(args.in_shp, 'r') as source:
        source_driver = source.driver
        source_crs = source.crs
        if "init" not in source_crs:
            print(f"force init to epsg:2154")
            source_crs["init"] = "epsg:2154"
            source.crs["init"] = "epsg:2154"
        print(f"source_crs {source_crs}")
        source_schema = source.schema
        lpis_parcels = [feat for feat in tqdm(source) if feat['geometry']]

    remove_fid = []
    for field in source_schema['properties']:
        if field in ["fid", "ogc_fid", "ogc_fid0"]:
            remove_fid.append(field)
    for field in remove_fid:
        del source_schema['properties'][field]
    for parcel in lpis_parcels:
        for field in remove_fid:
            del parcel['properties'][field]
    # step 2 create a spatial index for all subsequent spatial proximity query
    # and in the same loop select small polygon
    idx = index.Index()
    print(f" create spatial index and select small parcell that are not crop border/buffer strip")

    source_schema['properties'][foi_cat_prop] = 'str:25'
    for pos, in_feat in enumerate(tqdm(lpis_parcels)):
        if not in_feat['geometry']:
            lpis_parcels[pos]['properties'][foi_cat_prop] = "exclude"
            continue
        geom = shape(in_feat['geometry'])
        if not geom:
            lpis_parcels[pos]['properties'][foi_cat_prop] = "exclude"
            continue
        crop_code = in_feat['properties'][crop_fieldname]
        idx.insert(pos, geom.bounds)
        lpis_parcels[pos]['properties'][foi_cat_prop] = ""
        if geom.area > surf_min_m2:
            no_small_parcels_count += 1
            continue
        if crop_code in exclude_crop_code:
            lpis_parcels[pos]['properties'][foi_cat_prop] = "exclude"
            except_parcels_count += 1
            continue

        crop_uid = in_feat['properties'][uid_fieldname]
        small_parcels.append((pos, crop_uid))

    # print(f" lpis_parcels[0] : {lpis_parcels[0]}")
    print(f" number of small parcels (no borders) detected : {len(small_parcels)}")

    # step 3 select small parcel to add to a graph before FOI merging. We only keep
    # parcel/polygon that could be mergerd with another polygon. SO we exclude all isolated parcell (no neighbor in a
    # distance of 5m
    foi_fusion_parcels = []
    foi_fusion_parcels_n = []
    no_fusion_parcels = []
    print(f" select small parcels that could be merged with neighbour parcels")

    def check_parcel_overlap(parcel_geom, parcel_pos):
        query_bbox = parcel_geom.bounds
        for pos_id in idx.intersection(query_bbox):
            if pos_id == parcel_pos:
                continue
            feat_check = lpis_parcels[pos_id]
            geom_check = shape(feat_check['geometry'])

            if parcel_geom.within(geom_check):
                return "within"

            if geom_check.within(parcel_geom):
                return "contains"

            # check overlap between the 2 parcels.
            if parcel_geom.intersects(geom_check):
                geom_intersection = parcel_geom.intersection(geom_check)
                # if more of 30% area intersection
                if geom_intersection.area / parcel_geom.area > 0.3 or geom_intersection.area / geom_check.area > 0.3:
                    return "strong_overlap"

        return "checked"

    def add_parcel_node(lpis_graph, add_pos, neighbor_search_dist=5, foi_cat=foi_cat_prop, mask = None):
        add_feat = lpis_parcels[add_pos]
        add_crop_uid = add_feat['properties'][uid_fieldname]
        add_geom = shape(add_feat['geometry'])
        add_crop_code = add_feat['properties'][crop_fieldname]
        query_bbox = add_geom.buffer(neighbor_search_dist).bounds
        is_mergeable = False
        # if parcel already tag as an error and is not mergeable we stop here.
        if add_feat['properties'][foi_cat_prop] == "":
            add_feat['properties'][foi_cat_prop] = check_parcel_overlap(add_geom, add_pos)
            lpis_parcels[add_pos]['properties'][foi_cat_prop] = add_feat['properties'][foi_cat_prop]

        if add_feat['properties'][foi_cat_prop] in ["contains", "within", "strong_overlap"]:
            return is_mergeable

        for pos_id in idx.intersection(query_bbox):
            if pos_id == add_pos:
                continue
            if mask is not None:
                if pos_id in mask:
                    continue
            feat_neighbour = lpis_parcels[pos_id]
            geom_n = shape(feat_neighbour['geometry'])
            crop_code_n = feat_neighbour['properties'][crop_fieldname]
            buff_dist = 2.0
            geom_buff = add_geom.buffer(buff_dist)
            geom_n_buff = geom_n.buffer(buff_dist)
            # Check between the 2 parcels before adding them to graph
            #  * have the same crop code
            #  * are not inside each other. if it's the case we disabled this parcel for next processing.
            #  * are within a distance of 2 meter from each/other (bijectivity test)
            #  * one of the two is a small parcel, so have an area < surf_min_m2
            if add_crop_code != crop_code_n:
                continue
            if feat_neighbour['properties'][foi_cat_prop] == "":
                feat_neighbour['properties'][foi_cat_prop] = check_parcel_overlap(geom_n, pos_id)
                lpis_parcels[add_pos]['properties'][foi_cat_prop] = feat_neighbour['properties'][foi_cat_prop]

            if feat_neighbour['properties'][foi_cat_prop] in ["contains", "within", "strong_overlap"]:
                continue

            if not (geom_buff.intersects(geom_n) and geom_n_buff.intersects(add_geom)):
                continue
            if geom_n.area > surf_min_m2 and add_geom.area > surf_min_m2:
                continue

            # for case of very small rectangle parcel with width < 2 we must check that no parcel is beetween
            # geom and geom_n
            unary_geom_buff = unary_union([add_geom, geom_n]).buffer(buff_dist)
            unary_geom_bbox = unary_geom_buff.bounds
            inside_parcel = False
            mask_inside = [pos_id, add_pos]
            if mask is not None:
                mask_inside.extend(mask)
            for inside_id in idx.intersection(unary_geom_bbox):
                if inside_id in mask_inside:
                    continue
                feat_inside = lpis_parcels[inside_id]
                geom_inside = shape(feat_inside['geometry'])
                if (geom_inside.intersects(geom_buff)
                        and geom_inside.intersects(geom_n_buff)
                        and geom_inside.within(unary_geom_buff)):
                    inside_parcel = True
                    continue
            if inside_parcel:
                continue

            if feat_neighbour['properties'][foi_cat_prop] in ["contains", "within", "strong_overlap"]:
                continue

            if not is_mergeable:
                is_mergeable = True
                if add_geom.area > surf_min_m2:
                    lpis_graph.add_node(add_pos, small=False, uid=add_crop_uid, area=add_geom.area)
                else:
                    lpis_graph.add_node(add_pos, small=True, uid=add_crop_uid, area=add_geom.area)

            common_length = length_at_distance(geom_n, add_geom, 2)
            crop_uid_n = feat_neighbour['properties'][uid_fieldname]
            if geom_n.area > surf_min_m2:
                in_feat['properties'][foi_cat_prop] = "graph_node"
                lpis_graph.add_node(pos_id, small=False, uid=crop_uid_n, area=geom_n.area)
            else:
                lpis_graph.add_node(pos_id, small=True, uid=crop_uid_n, area=geom_n.area)
            if add_geom.area <= surf_min_m2:
                lpis_graph.add_edge(add_pos, pos_id, weight=common_length)
            if geom_n.area <= surf_min_m2:
                lpis_graph.add_edge(pos_id, add_pos, weight=common_length)

        if is_mergeable:
            lpis_parcels[pos]['properties'][foi_cat_prop] = "mergeable"
        else:
            if in_feat['properties'][foi_cat_prop] == "":
                lpis_parcels[pos]['properties'][foi_cat_prop] = "mergeable"
        return is_mergeable

    # build a relation spatial graph with small parcel as node seed.
    # add edge and node from all parcels that could be merged with the node seeds.
    G = nx.Graph()
    for pos, crop_uid in tqdm(small_parcels):
        merge_enabled = add_parcel_node(G, pos, 5, foi_cat_prop)
        if not merge_enabled:
            no_fusion_parcels.append((pos, crop_uid))
        else:
            foi_fusion_parcels.append((pos, crop_uid))
            for neighbor, edge_info in G[pos].items():
                foi_fusion_parcels_n.append((neighbor, G.nodes[neighbor]["uid"], crop_uid, edge_info["weight"]))

        # print(f"crop {crop_uid} candidat for foi processing, surf {geom.area:03.2f} code {crop_code}")

    print(f" number of small parcels candidat to foi {len(foi_fusion_parcels)}")

    if "SURF_ADM" in source_schema['properties']:
        source_schema['properties']["SURF_ADM"] = 'float'
    if "surf_adm" in source_schema['properties']:
        source_schema['properties']["surf_adm"] = 'float'
    dest_schema = copy.deepcopy(source_schema)
    print(f"dest schema \n{dest_schema}")

    # step 3b save temporary data for analysis
    out_no_fusion_parcel_shp = os.path.join(work_dir, basename+"_small_without_neighbor.shp")
    print(f"save small parcel that could not be fusioned in {out_no_fusion_parcel_shp}")
    with fiona.open(out_no_fusion_parcel_shp, 'w', driver=source_driver,
                    crs=source_crs, schema=dest_schema) as out_layer:
        for pos, crop_uid in tqdm(no_fusion_parcels):
            in_feat = lpis_parcels[pos]
            out_layer.write(in_feat)

    out_fusion_parcel_shp = os.path.join(work_dir, basename+"_small_with_neighbor.shp")
    print(f"save small parcel that could be fusioned in {out_fusion_parcel_shp}")
    with fiona.open(out_fusion_parcel_shp, 'w', driver=source_driver, crs=source_crs,
                    schema=dest_schema) as out_layer:
        for pos, crop_uid in tqdm(foi_fusion_parcels):
            in_feat = lpis_parcels[pos]
            out_layer.write(in_feat)

    out_overlap_parcel_shp = os.path.join(work_dir, basename+"_overlap_error.shp")
    print(f"save parcels excluded from processing because they contains or are within other parcels "
          f"{out_overlap_parcel_shp}")
    err_count = 0
    with fiona.open(out_overlap_parcel_shp, 'w', driver=source_driver, crs=source_crs,
                    schema=dest_schema) as out_layer:
        for feat in tqdm(lpis_parcels):
            # print(f"{feat}")
            if feat['properties'][foi_cat_prop] in ["contains", "within", "strong_overlap"]:
                err_count +=1
                out_layer.write(feat)
    print(f"{err_count} overlap error found")
    
    out_fusion_parcel_n_shp = os.path.join(work_dir, basename+"_small_neighbor.shp")
    dest_schema['properties']["uid_n"] = dest_schema['properties'][uid_fieldname]
    dest_schema['properties']["combord_n"] = 'float'
    print(f" save parcels that could be fusionned with small in {out_fusion_parcel_n_shp}")
    with fiona.open(out_fusion_parcel_n_shp, 'w', driver=source_driver, crs=source_crs,
                    schema=dest_schema) as out_layer:
        for pos, crop_uid, crop_uid_neighbor, common_length_dist in tqdm(foi_fusion_parcels_n):
            in_feat = copy.deepcopy(lpis_parcels[pos])
            in_feat['properties']["uid_n"] = crop_uid_neighbor
            in_feat['properties']["combord_n"] = common_length_dist
            out_layer.write(in_feat)

    # step 4 process FOI merging. Parcel is merge by connected components.

    connect_comp = nx.connected_components(G)
    connect_comp_nb = nx.number_connected_components(G)
    print(f" number of graph connected componenents {connect_comp_nb}")

    def merged_feat_foi(small_feature, neighbor_feature):
        small_geom = shape(small_feature['geometry'])
        neighbor_geom = shape(neighbor_feature['geometry'])
        foi_geom = unary_union([small_geom, neighbor_geom])
        if foi_geom.geom_type == "Polygon":
            foi_geom = MultiPolygon([foi_geom])

        foi_feature = {
            'geometry': mapping(foi_geom),
            'properties': {
                "surf_adm": small_feature['properties']["surf_adm"] + neighbor_feature['properties']["surf_adm"],
                crop_fieldname: small_feature['properties'][crop_fieldname],
                foi_cat_prop: "foi"
            }
        }
        return foi_feature

    def parcel_weighted_distance():
        return 0

    cc_count = 0
    merged_begin_pos = len(lpis_parcels) - 1
    foi_no_merge = []
    foi_small_merge = []
    foi_features = []
    merged_features = []
    largest_cc = max([G.subgraph(c) for c in nx.connected_components(G)], key=len)
    max_cc_size = len(largest_cc)
    print(f" max connected component size {max_cc_size}")

    for c in tqdm(nx.connected_components(G)):
        cc_count += 1
        subgraph = G.subgraph(c).copy()
        all_nodes = subgraph.nodes(data=True)
        small_nodes = [node for node in all_nodes if node[1]["small"]]
        small_nodes_id = [node[0] for node in small_nodes]
        small_nodes_uid = [node[1]['uid'] for node in small_nodes]

        verbose = False
        if any(x in small_nodes_uid for x in verbose_fid):
            all_nodes_uid = [node[1]['uid'] for node in all_nodes]
            print(f" connected component {cc_count}, uid small list {small_nodes_uid}")
            print(f" all initial nodes id : {all_nodes_uid}")
            verbose = True

        sorted(small_nodes, key=lambda node: node[1]["area"])
        iter = 0
        merge_count = 0
        # print(f" connected_components {cc_count}, nb small_nodes {len(small_nodes)}")
        mask_feat = []
        while len(small_nodes) and iter < 2*max_cc_size:
            iter += 1
            node = small_nodes.pop(0)
            # verbose = False
            # if node[1]['uid']  in  ["0681580430008008", "0680106620009009"]:
            #    verbose = True
            if verbose:
                print(f"node pos {node[0]}, uid {node[1]['uid']}")
            if verbose:
                small_nodes_uid = [node[1]['uid'] for node in small_nodes]
                print(f"small uid {small_nodes_uid}")

            small_nodes_edges = subgraph.edges(node[0], data=True)
            if verbose:
                all_nodes_subgraph = subgraph.nodes(data=True)
                all_nodes_uid = [node[1]['uid'] for node in all_nodes]
                print(f"   subgraph current nodes {all_nodes_uid}")

            min_fus_weight = 40
            max_weight = min_fus_weight
            node_fus_pos = None
            for edge in small_nodes_edges:
                # print(f" edge {edge}")
                if edge[2]["weight"] >= min_fus_weight and edge[2]["weight"] > max_weight:
                    max_weight = edge[2]["weight"]
                    node_fus_pos = edge[1]

            if node_fus_pos is not None:
                if verbose:
                    print(f"   node_fus : id {subgraph.nodes[node_fus_pos]['uid']} pos {node_fus_pos}")

                merge_count += 1
                small_feat = lpis_parcels[node[0]]
                neighbor_feat = lpis_parcels[node_fus_pos]
                foi_feat = merged_feat_foi(small_feat, neighbor_feat)
                uid = f"999{cc_count:010}{merge_count:03}"
                foi_feat['properties'][uid_fieldname] = uid
                merged_geom = shape(foi_feat['geometry'])
                lpis_parcels.append(foi_feat)
                merged_feat_pos = len(lpis_parcels) - 1
                idx.insert(merged_feat_pos, merged_geom.bounds)

                subgraph.remove_node(node[0])
                subgraph.remove_node(node_fus_pos)
                mask_feat.extend([node[0], node_fus_pos])
                merge_enabled = add_parcel_node(
                    subgraph, merged_feat_pos, 5, foi_cat_prop, mask=mask_feat)

                if verbose:
                    all_nodes_subgraph = subgraph.nodes(data=True)
                    all_nodes_uid = [node[1]['uid'] for node in all_nodes]
                    print(f"   subgraph after add foi nodes {all_nodes_uid}")

                if verbose:
                    print(f"   merge_enabled {merge_enabled}")

                if merge_enabled:
                    foi_features.append(merged_feat_pos)
                else:
                    if verbose:
                        print(f"   merged_geom.area {merged_geom.area}")
                    if merged_geom.area < surf_min_m2:
                        if verbose:
                            print(f"   append {merged_feat_pos} to foi_small_merge")
                        foi_small_merge.append(merged_feat_pos)
                    else:
                        if verbose:
                            print(f"   append {merged_feat_pos} to foi_features")
                        foi_features.append(merged_feat_pos)

                # print(f" small_nodes size {len(small_nodes)}")

                if node[0] in foi_features:
                    foi_features.remove(node[0])
                if node_fus_pos in foi_features:
                    foi_features.remove(node_fus_pos)

            else:
                if verbose:
                    print(f"   node_fus is None")

                if node[0] <= merged_begin_pos:
                    if verbose:
                        print(f"   append {node[1]} pos {node[0]} to foi_no_merge")
                    foi_no_merge.append(node[0])
                else:
                    if verbose:
                        print(f"   append {node[1]} pos {node[0]}  to foi_small_merge")
                    foi_small_merge.append(node[0])
                if verbose:
                    print(f"   remove {node[1]} pos {node[0]} subgraph")
                subgraph.remove_node(node[0])
                mask_feat.append(node[0])
                if verbose:
                    all_nodes_subgraph = subgraph.nodes(data=True)
                    all_nodes_uid = [node[1]['uid'] for node in all_nodes]
                    print(f"   subgraph node after update {all_nodes_uid}")

                if node[0] in foi_features:
                    foi_features.remove(node[0])
                # print(f" subgraph after remove nodes no merge {subgraph.nodes}")
                # print(f" small_nodes size {len(small_nodes)}")

            # update subgraph
            all_nodes = subgraph.nodes(data=True)
            small_nodes = [node for node in all_nodes if node[1]["small"]]
            sorted(small_nodes, key=lambda node: node[1]["area"])

        if iter == 100:
            print(f"warning nb iter grow up to 100")
        merged_features.extend(mask_feat)

    out_no_foi_parcel_shp = os.path.join(work_dir, basename+"_small_no_merge.shp")
    dest_schema = copy.deepcopy(source_schema)
    print(f" save small parcels that could not be fusionned with their neighbor of same code  {out_no_foi_parcel_shp}")
    with fiona.open(out_no_foi_parcel_shp, 'w', driver=source_driver, crs=source_crs,
                    schema=dest_schema) as out_layer:
        for pos in tqdm(foi_no_merge):
            in_feat = lpis_parcels[pos]
            out_layer.write(in_feat)

    out_foi_parcel_shp = os.path.join(work_dir, basename+"_out_foi_ok.shp")
    print(f" save small parcels merged with their neighbor of same code  {out_foi_parcel_shp}")
    foi_schema = source_schema.copy()
    foi_schema['geometry'] = 'MultiPolygon'
    foi_schema['properties'] = dict()
    foi_schema['properties']["surf_adm"] = 'float'
    foi_schema['properties'][crop_fieldname] = source_schema['properties'][crop_fieldname]
    foi_schema['properties'][uid_fieldname] = source_schema['properties'][uid_fieldname]
    foi_schema['properties'][foi_cat_prop] = source_schema['properties'][foi_cat_prop]

    with fiona.open(out_foi_parcel_shp, 'w', driver=source_driver, crs=source_crs,
                    schema=foi_schema) as out_layer:
        for pos_foi in tqdm(foi_features):
            foi_feat = lpis_parcels[pos_foi]
            out_layer.write(foi_feat)

    out_foi_parcel_shp = os.path.join(work_dir, basename+"_out_foi_small.shp")
    with fiona.open(out_foi_parcel_shp, 'w', driver=source_driver, crs=source_crs,
                    schema=foi_schema) as out_layer:
        for pos_foi in tqdm(foi_small_merge):
            foi_feat = lpis_parcels[pos_foi]
            out_layer.write(foi_feat)

    # step 5 create sqlite db with all data and do post processing for stat

    # step 5a init sqlite db with input lpis data

    sqlite_db_name = os.path.join(work_dir, basename+"_foi.sqlite")
    print("create sqlite and import in_lpis : {0}".format(args.in_shp))
    sqlite_init_args = [
        "ogr2ogr", "-f", "SQLite", "-dsco", "SPATIALITE=YES", sqlite_db_name, args.in_shp, "-nlt", "PROMOTE_TO_MULTI",
        "-nln", basename, "-lco", "SPATIAL_INDEX=YES"]
    subprocess.call(sqlite_init_args)

    # step 5b load all temporary shapefile in sqlite db
    for shp_suffix in ["_small_without_neighbor", "_small_with_neighbor", "_small_neighbor", "_small_no_merge",
                       "_out_foi_ok", "_out_foi_small"] :
        in_shp = os.path.join(work_dir, basename+shp_suffix+".shp")
        print("append {0} to sqlite database".format(in_shp))

        sqlite_append_args = [
           "ogr2ogr", "-append", sqlite_db_name, in_shp, "-nln", basename+shp_suffix, "-nlt", "PROMOTE_TO_MULTI",
           "-lco", "SPATIAL_INDEX=YES"]
        subprocess.call(sqlite_append_args)

    # step 5c load all temporary shapefile in sqlite db
    conn = sqlite3.connect(sqlite_db_name)
    try:
        conn.enable_load_extension(True)
        conn.load_extension("mod_spatialite")
        uidf = uid_fieldname
        in_table = basename+"_out_foi_ok"
        print(f"create buffer from {in_table}")
        sql = "CREATE TABLE {0} AS ".format(in_table+"_dilat")
        sql += f"SELECT {uidf}, CastToMultiPolygon(ST_Buffer(ST_Buffer(p.GEOMETRY, 0.5), -0.3)) AS GEOMETRY "
        sql += "FROM {0} p; ".format(in_table)
        conn.execute(sql)
        sql = "SELECT RecoverGeometryColumn('{0}', 'GEOMETRY', 2154, 'MULTIPOLYGON', 'XY')".format(in_table+"_dilat")
        conn.execute(sql)
        sql = "SELECT CreateSpatialIndex('{0}', 'GEOMETRY');".format(in_table+"_dilat")
        conn.execute(sql)

        in_table = basename + "_out_foi_small"
        print(f"create buffer from {in_table}")
        sql = "CREATE TABLE {0} AS ".format(in_table + "_dilat")
        sql += f"SELECT {uidf}, CastToMultiPolygon(ST_Buffer(ST_Buffer(p.GEOMETRY, 0.5), -0.3)) AS GEOMETRY "
        sql += "FROM {0} p; ".format(in_table)
        conn.execute(sql)
        sql = "SELECT RecoverGeometryColumn('{0}', 'GEOMETRY', 2154, 'MULTIPOLYGON', 'XY')".format(in_table + "_dilat")
        conn.execute(sql)
        sql = "SELECT CreateSpatialIndex('{0}', 'GEOMETRY');".format(in_table+"_dilat")
        conn.execute(sql)

        print(f"create index")
        index_tables_suff = ["_small_without_neighbor", "_small_with_neighbor", "_small_neighbor", "_small_no_merge"]
        for suffix in index_tables_suff:
            sql = "CREATE INDEX {0}{1}_index_id ON {0}{1}({2});".format(basename, suffix, uidf)
            conn.execute(sql)

        no_merge = basename+"_small_no_merge"
        out_merged_ok = basename+"_small_merged_ok"
        small_with_neighbor = basename+"_small_with_neighbor"
        foi_ok_dilat = basename+"_out_foi_ok_dilat"
        print(f"create {out_merged_ok} with spatial sql query")
        sql = "CREATE TABLE {0} AS ".format(out_merged_ok)
        sql += "SELECT p. * FROM {0} p, {1} foi LEFT OUTER JOIN {2} n ON p.{3} = n.{3} ".format(
            small_with_neighbor, foi_ok_dilat, no_merge, uidf)
        sql += "WHERE ST_Within(p.GEOMETRY, foi.GEOMETRY) = 1 "
        sql += "AND p.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = '{0}' ".format(small_with_neighbor)
        sql += "AND search_frame = foi.GEOMETRY) "
        sql += f"AND n.{uidf} IS null "
        print(f"test within query {sql} ")
        conn.execute(sql)
        sql = "SELECT RecoverGeometryColumn('{0}', 'GEOMETRY', 2154, 'MULTIPOLYGON', 'XY')".format(out_merged_ok)
        conn.execute(sql)
        sql = "SELECT CreateSpatialIndex('{0}', 'GEOMETRY');".format(out_merged_ok)
        conn.execute(sql)
        sql = "CREATE INDEX {0}_index_id ON {0}({1});".format(out_merged_ok, uidf)
        conn.execute(sql)

        out_merged_small = basename+"_small_merged_small"
        foi_small_dilat = basename+"_out_foi_small_dilat"
        print(f"create {out_merged_small} with spatial sql query")
        sql = "CREATE TABLE {0} AS ".format(out_merged_small)
        sql += "SELECT p. * FROM {0} p, {1} foi LEFT OUTER JOIN {2} n ON p.{3} = n.{3} ".format(
            small_with_neighbor, foi_small_dilat, no_merge, uidf)
        sql += "WHERE ST_Within(p.GEOMETRY, foi.GEOMETRY) = 1 "
        sql += "AND p.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = '{0}' ".format(small_with_neighbor)
        sql += "AND search_frame = foi.GEOMETRY) "
        sql += f"AND n.{uidf} IS null "
        print(f"test within query {sql} ")
        conn.execute(sql)
        sql = "SELECT RecoverGeometryColumn('{0}', 'GEOMETRY', 2154, 'MULTIPOLYGON', 'XY')".format(out_merged_small)
        conn.execute(sql)
        sql = "SELECT CreateSpatialIndex('{0}', 'geometry');".format(out_merged_small)
        conn.execute(sql)
        sql = "CREATE INDEX {0}_index_id ON {0}({1});".format(out_merged_small, uidf)
        conn.execute(sql)

        out_foi_ok = basename + "_out_foi_ok"
        foi_cleanup = basename+"_out_foi_ok_cleanup"
        print(f"try cleanup foi from {out_foi_ok}")
        sql = "CREATE TABLE {0} AS ".format(foi_cleanup)
        sql += f"SELECT {uidf}, "
        sql += "CastToMultiPolygon(ST_Union(ST_Buffer(ST_Buffer(p.GEOMETRY, 2), -2.2), GEOMETRY)) AS GEOMETRY "
        sql += "FROM {0} p; ".format(out_foi_ok)
        conn.execute(sql)
        sql = "SELECT RecoverGeometryColumn('{0}', 'GEOMETRY', 2154, 'MULTIPOLYGON', 'XY')".format(foi_cleanup)
        conn.execute(sql)
        sql = "SELECT CreateSpatialIndex('{0}', 'GEOMETRY');".format(foi_cleanup)
        conn.execute(sql)

    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()

    # step 5d export parcelle that have been merged into a FOI as shapefile
    print(f"export shapefile")
    no_merge = basename + "_small_no_merge"
    out_merged_ok = basename + "_small_merged_ok"
    out_merged_small = basename + "_small_merged_small"
    out_foi_small_dilat = basename + "_out_foi_small_dilat"
    out_foi_ok_dilat = basename + "_out_foi_ok_dilat"
    foi_cleanup = basename + "_out_foi_ok_cleanup"

    for table in [out_merged_ok, out_merged_small, out_foi_small_dilat, out_foi_ok_dilat, foi_cleanup]:
        print(f"export {table}")
        sql_query = "SELECT {0}.* FROM {0}".format(table)
        sqlite_extract_args = [
            "ogr2ogr", "-f", "ESRI Shapefile", "-dialect", "sqlite", "-sql", """{0}""".format(sql_query),
            os.path.join(work_dir, table+".shp"), sqlite_db_name]
        subprocess.call(sqlite_extract_args)

    # step 6 export some statistiques
    print(f"calc stat")

    conn = sqlite3.connect(sqlite_db_name)
    try:
        conn.enable_load_extension(True)
        conn.load_extension("mod_spatialite")
        curr = conn.cursor()

        in_table = basename
        no_merge = basename+"_small_no_merge"
        without_neighbor = basename+"_small_without_neighbor"
        out_merged_small = basename+"_small_merged_small"
        foi_small_dilat = basename+"_out_foi_small_dilat"
        out_merged_ok = basename+"_small_merged_ok"
        small_with_neighbor = basename+"_small_with_neighbor"
        foi_small = basename + "_out_foi_small"
        foi_ok = basename + "_out_foi_small"
        len_prefix = len(basename)+1

        sql = "SELECT count(*) FROM {0} WHERE ST_Area(GEOMETRY) < 5000 AND ".format(basename)
        exclude_crop_code_str = ', '.join('"{0}"'.format(code) for code in exclude_crop_code)
        sql += "{0} NOT IN ({1})".format(crop_fieldname, exclude_crop_code_str)
        print(f"count small query \n {sql}")
        curr.execute(sql)
        count = curr.fetchone()[0]

        df = pd.DataFrame(data={'small': count}, index=pd.Index([basename, ], name="S2_Tile"))
        for table in [without_neighbor, small_with_neighbor, no_merge, out_merged_small, out_merged_ok]:
            sql = "SELECT count(*) FROM {0} ".format(table)
            curr.execute(sql)
            count = curr.fetchone()[0]
            df[table[len_prefix:]] = pd.Series([count,] , index=df.index)

        df["all_merge_no_merge"] = df["small_no_merge"]+df["small_merged_small"]+df["small_merged_ok"]
        df["with_and_without"] = df["small_with_neighbor"]+df["small_without_neighbor"]
        df.to_csv(os.path.join(work_dir, basename+"_stat.csv"), index=True, sep=";")
        print(f"stats\n {df.T.head(10)}")

    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()

    # step 7 execute some out data quality check
    # check for duplicate on out table produced by iterative mergin: i.e no_merge, merged_small, merged_ok
    # check for overlap bewteen each couple of theses tables
    conn = sqlite3.connect(sqlite_db_name)
    try:
        conn.enable_load_extension(True)
        conn.load_extension("mod_spatialite")
        curr = conn.cursor()

        no_merge = basename+"_small_no_merge"
        out_merged_small = basename+"_small_merged_small"
        out_merged_ok = basename+"_small_merged_ok"
        without_neighbor = basename+"_small_without_neighbor"

        check_tables = [no_merge, out_merged_small, out_merged_ok]
        for table in check_tables:
            print(f"check duplicate from {table}")
            sql = f"SELECT DISTINCT {uidf} FROM {table}\n"
            sql += f"WHERE {uidf} IN (SELECT {uidf} FROM {table} GROUP BY {uidf} HAVING COUNT( *) > 1)\n"
            curr.execute(sql)
            duplicate_id_tuple = curr.fetchmany()
            if len(duplicate_id_tuple) > 0:
                uid_list = [id_tuple[0] for id_tuple in duplicate_id_tuple]
                print(f"ERROR duplicate in table {table} ids :\n  {uid_list}")
            else:
                print(f"OK no duplicate in table {table}")

        for table_a, table_b in itertools.combinations(check_tables, 2):
            print(f"check overlap id from {table_a} and {table_b}")
            sql = f"SELECT {table_a}.{uidf} FROM {table_a}\n"
            sql += f"INNER JOIN {table_b} ON {table_a}.{uidf}={table_b}.{uidf}"
            curr.execute(sql)
            overlap_id_tuple = curr.fetchmany()
            if len(overlap_id_tuple) > 0:
                uid_list = [id_tuple[0] for id_tuple in overlap_id_tuple]
                print(f"ERROR overlap  between tables {table_a} and {table_b} ids:\n   {uid_list}")
            else:
                print(f"OK no overlap between tables {table_a} and {table_b}")

        for table in check_tables:
            print(f"check overlap id from {table} and {without_neighbor}")
            sql = f"SELECT {table}.{uidf} FROM {table}\n"
            sql += f"INNER JOIN {without_neighbor} ON {table}.{uidf}={without_neighbor}.{uidf}\n"
            curr.execute(sql)
            overlap_id_tuple = curr.fetchmany()
            if len(overlap_id_tuple) > 0:
                uid_list = [id_tuple[0] for id_tuple in overlap_id_tuple]
                print(f"ERROR overlap  between tables {table} and {without_neighbor} ids:\n   {uid_list}")
            else:
                print(f"OK no overlap between tables {table} and {without_neighbor}")

    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()
