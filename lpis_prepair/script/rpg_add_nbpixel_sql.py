#!/usr/bin/env python
# coding: utf-8


#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging
import time
import os
import sys
import sqlite3
import numpy as np

from pyproj import Transformer as pj_Transformer
import rasterio
from rasterio import features
from affine import Affine
from shapely import wkt
from shapely.geometry import mapping, shape
from shapely.ops import transform as shape_transform
import mgrs as mgrs

from rasterstats import zonal_stats
from tqdm import tqdm


def compute_mask(feature_collection, s2_tile, res, out_raster=None):
    """
    compute mask from features using envelop of a base raster

    :param feature_collection: features to burn
    :param s2_tile: raster to use a reference for out envelop/georef
    :param res: resolution to use
    :param out_raster: out raster fullname
    :return: fullname
    """
    if res not in [10, 20, 60]:
        raise ValueError("s2 tile resolution must be in [10,20,60]")

    out_shape_dict = {
        10: [10980, 10980],
        20: [5490, 5490],
        60: [1830, 1830]}

    out_shape = out_shape_dict[res]
    # print(" out_shape : {0}".format(out_shape))

    mg = mgrs.MGRS()
    zone, hemi, x_tile_coord, y_tile_coord = mg.MGRSToUTM(f"{args.s2_tile}0000099999".encode())
    if hemi == b'N':
        epsg_code = f"epsg:326{zone}"
    else:
        epsg_code = f"epsg:327{zone}"
    out_crs = epsg_code

    print(f"x_tile {x_tile_coord}")
    print(f"y_tile {y_tile_coord}")

    y_s2_tile = y_tile+1+20
    geotransform = (x_tile, res*1.0, 0.0, y_s2_tile, 0.0, -res*1.0)
    out_affine = Affine.from_gdal(*geotransform)
    print(" out_affine :\n{0}".format(out_affine))

    # print(" out_meta : {0}".format(out_meta))

    feat_gen = ((feature["geometry"], 1) for feature in feature_collection if feature["geometry"] is not None)
    image = features.rasterize(feat_gen, out_shape=out_shape, transform=out_affine)

    # print(image.shape)
    if out_raster is not None:
        with rasterio.open(out_raster, "w", driver='GTiff', dtype=rasterio.uint8, count=1, width=out_shape[1],
                          height=out_shape[0], compress='lzw', transform=out_affine, crs=out_crs ) as dest:
            print(dest.width, dest.height)
            print(dest.crs)
            print(dest.transform)
            print(dest.count)
            print(dest.indexes)
            dest.write(image, 1)

    return image, out_affine


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="compute for each polygone feature the number of pixel inside the feature from a img_tile mask")

    parser.add_argument("-i", "--in_db", dest="in_db", help="spatialite_db", required=True)
    parser.add_argument("-t", "--in_table", dest="in_table", help="table to process", required=True)
    parser.add_argument(
        "-s2", "--s2_tile", dest="s2_tile", help="name of the S2 Tile", required=True)
    parser.add_argument(
        "-of", "--out_field", dest="out_field", help="name of the output filed containing the number of pixel",
        required=False)
    parser.add_argument(
        "-if", "--id_field", dest="id_field", help="name of the output filed containing the number of pixel",
        required=False)
    parser.add_argument(
        "-b", "--buffer_size", dest="buffer_size", help="buffer size to apply for geometry erosion", type=float,
        required=False)
    parser.add_argument(
        "-r", "--resolution", dest="resolution", help="pixle resolution must be in 10,20,60", type=float,
        required=True)
    parser.add_argument(
        "-m", "--out_mask", dest="out_mask", help="name of the output image mask", required=False)
    parser.add_argument(
        "-n", "--num_max", dest="num_max", help="split processing by num_max", type=int, required=False)
    parser.add_argument('--no-out_mask', dest='keep_mask', action='store_false')
    parser.set_defaults(keep_mask=True)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    # 1 load input feature inside feature collection /geosjon and apply negative buffer is required
    conn = sqlite3.connect(args.in_db)
    conn.enable_load_extension(True)
    try:
        conn.load_extension("mod_spatialite")
    except:
        print("load_extension(mod_spatialite) failed, Error: ", sys.exc_info()[1], "occured.")
        print('\n\nTry loading using the name of the dll (spatialite.dll)')
        conn.load_extension("spatialite")

    uid_field = "ogc_fid"
    if args.id_field:
        uid_field = args.id_field

    cur = conn.cursor()
    query_crs = f"SELECT srid FROM geometry_columns WHERE f_table_name = '{args.in_table}';"
    cur.execute(query_crs)
    rows = cur.fetchall()
    print(f"curr_crs {rows}")
    feat_crs = f"epsg:{rows[0][0]}"
    print(f"feat_crs {feat_crs}")

    query_count = f"SELECT Count(*)  FROM {args.in_table}"
    cur.execute(query_count)
    rows = cur.fetchall()
    print(f"count_table {rows[0][0]}")
    count_table = int(rows[0][0])

    print("query data")
    curr_row = cur.execute(f"SELECT {uid_field}, AsEWKT(GEOMETRY) FROM {args.in_table} WHERE ST_IsValid(GEOMETRY)")
    cols = [column[0] for column in curr_row.description]

    if args.num_max:
        fetch_nb = args.num_max
    else:
        fetch_nb = count_table
        
    block_nb = 0
    process_nb = 0
    rows_all = curr_row.fetchall()
    mask_list = []
    while process_nb < count_table:
        min_fetch = block_nb*fetch_nb
        max_fetch = (block_nb+1)*fetch_nb
        if max_fetch > count_table:
            max_fetch = None
        rows = rows_all[min_fetch:max_fetch]
        if len(rows) == 0:
            break
        block_nb += 1

        print("load data")
        feat_collection = []
        for row in rows:
            feat = dict()
            feat['properties'] = dict()
            geom = wkt.loads(row[1].split(";")[1])
            if not geom.is_valid:
                continue
            feat["geometry"] = mapping(geom)
            feat['properties'][uid_field] = row[0]
            feat["geometry_ini"] = feat["geometry"]
            feat['type'] = "Feature"
            feat_collection.append(feat)

        if args.buffer_size is not None:
            print(f"begin buffer")
            start_buff_time = time.time()
            for feat in tqdm(feat_collection):
                geom_buff = shape(feat['geometry']).buffer(-args.buffer_size)
                if not geom_buff:
                    feat["geometry"] = None
                    continue
                if not geom_buff.is_valid:
                    feat["geometry"] = None
                    continue

                if geom_buff.is_empty:
                    feat["geometry"] = mapping(shape(feat['geometry']).representative_point())
                    continue
                feat["geometry"] = mapping(geom_buff)

            print("end buffer : {0:.3f}s".format(time.time() - start_buff_time))

        # # 2 compute a image binary mask from input feature and from input image bounds & resolutions
        m = mgrs.MGRS()
        utm_zone, utm_hemi, x_tile, y_tile = m.MGRSToUTM(f"{args.s2_tile}0000099999".encode())
        print(f"utm_zone {utm_zone}")
        print(f"utm_hemi {utm_hemi}")
        if utm_hemi == b'N':
            utm_epsg = f"epsg:326{utm_zone}"
        else:
            utm_epsg = f"epsg:327{utm_zone}"
        tile_crs = utm_epsg
        print(f"tile_crs {utm_epsg}")

        # reproject geom to correct utm zone
        if tile_crs != feat_crs:
            pj_transformer = pj_Transformer.from_crs(feat_crs, tile_crs)
            project = pj_transformer.transform
            # project = partial(
            #    pj_transform,
            #    Proj(**feat_crs),  # source coordinate system
            #    Proj(init=img_crs))  # destination coordinate system
            print(f"begin reproj")
            start_proj_time = time.time()
            for feat in tqdm(feat_collection):
                if not feat["geometry"]:
                    continue
                try:
                    feat["geometry"] = mapping(shape_transform(project, shape(feat['geometry'])))
                except IndexError:
                    # print(feat['geometry'])
                    feat["geometry"] = None
            print("end reproj : {0:.3f}s".format(time.time() - start_proj_time))

        in_dir, in_basename = os.path.split(args.in_db)
        if args.out_mask:
            out_mask_name = args.out_mask[:-4]
            out_mask_name = f"{out_mask_name}_{block_nb}.tif"
        else:
            in_basename = os.path.splitext(in_basename)[0]
            out_mask_name = f"{args.in_table}_mask_{block_nb}.tif"
        out_mask, affine = compute_mask(
            feat_collection, args.s2_tile, args.resolution, os.path.join(in_dir, out_mask_name))
        mask_list.append(os.path.join(in_dir, out_mask_name))

        # 3 compute nb pixel
        out_stat = zonal_stats(feat_collection, out_mask, affine=affine, stats=['count',])
        out_stat_iter = iter(out_stat)

        # 4 write output pixel
        out_field = "count"
        if args.out_field is not None:
            out_field = args.out_field

        query_exist = f"SELECT COUNT(*) AS CNTREC FROM pragma_table_info('{ args.in_table}') WHERE name = '{out_field}'"
        cur.execute(query_exist)
        rows = cur.fetchall()
        if rows[0][0] == 0:
            query_create_column = f"ALTER TABLE {args.in_table} ADD {out_field} INTEGER;"
            cur.execute(query_create_column)

        conn.execute('BEGIN TRANSACTION')
        count = 0
        for feat in tqdm(feat_collection):
            count += 1
            stat = next(out_stat_iter)
            count = stat['count']
            uid = feat['properties'][uid_field]

            if count % 10000 == 0:
                # print(count)
                conn.execute('COMMIT')
                conn.execute('BEGIN TRANSACTION')

            query_update = f"UPDATE {args.in_table} SET {out_field} = {count} WHERE {uid_field} = {uid};"
            conn.execute(query_update)

        conn.execute('COMMIT')
        conn.commit()

        process_nb += fetch_nb
        print(f"processed feature : {process_nb}")

    in_dir, in_basename = os.path.split(args.in_db)
    if args.out_mask:
        out_mask_name = args.out_mask[:-4]
        out_mask_name = f"{out_mask_name}.tif"
    else:
        in_basename = os.path.splitext(in_basename)[0]
        out_mask_name = f"{args.in_table}_mask.tif"

    with rasterio.open(mask_list[0], 'r') as one:
        mask_array = one.read(1).astype(bool)
        mask_affine = one.transform
        mask_crs = one.crs
        mask_shape = one.shape
        for mask in mask_list[1:]:
            with rasterio.open(mask, 'r') as tmp_mask:
                update_array = tmp_mask.read(1).astype(bool)
                mask_array = np.logical_or(mask_array,update_array)

        with rasterio.open(os.path.join(in_dir, out_mask_name), "w", driver='GTiff', dtype=rasterio.uint8, count=1,
                           width=mask_shape[1], height=mask_shape[0], compress='lzw', transform=mask_affine,
                           crs=mask_crs) as dest:
            dest.write(mask_array.astype(np.uint8), 1)

    for mask in mask_list:
        os.remove(mask)

    if not args.keep_mask:
        os.remove(os.path.join(in_dir, out_mask_name))

    cur.close()
