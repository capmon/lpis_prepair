#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging

import os
import sys
import glob
import shutil

import fiona
from shapely.geometry import shape
import zipfile
import subprocess


def get_shp_list(in_dir, name_filter=None):
    """
    return a list of shapefile in a directory, shapefile could be in zip archive

    :param in_dir:
    :return:
    """
    # first list all shapefile
    all_shapefiles = glob.glob(os.path.join(in_dir, "*.shp"))
    if name_filter:
        in_shapefiles = [f for f in all_shapefiles if name_filter in f]
    else:
        in_shapefiles = all_shapefiles
    print("find {0} shapefile".format(len(in_shapefiles)))

    # then list all zip file
    all_zip_archives = glob.glob(os.path.join(in_dir, "*.zip"))
    if name_filter:
        in_zip_archives = [f for f in all_zip_archives if name_filter in f]
    else:
        in_zip_archives = all_zip_archives
    print("find {0} archives".format(len(in_zip_archives)))
    
    in_zip_shapefiles = []
    for archive in  in_zip_archives:
        zip = zipfile.ZipFile(archive)
        # available files in the container
        zip_files = zip.namelist()
        zip_shp = [filename for filename in zip_files 
                   if filename[-3:] == "shp"]
        for shp in zip_shp:
           in_zip_shapefiles.append(os.path.join(archive,shp))

    print("find {0} shp in archives".format(len(in_zip_shapefiles)))

    return in_shapefiles, in_zip_shapefiles


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="merge french lpis, rpg, from department shapefile to a global shapefile or spatialite")

    parser.add_argument("-i", "--in_rpg_dir", dest="rpg_dir",
                        help="root directory containing all rph shapefile or zip archive", required=True)
    parser.add_argument("-o", "--out_name", dest="out_name", help="out filename",
                        required=True)
    parser.add_argument("-t", "--out_table_name", dest="out_table_name", help="out table",
                        required=True)
    parser.add_argument("-f", "--in_filter", dest="in_filter", help="filter for shapefile",
                        required=False)
    parser.add_argument("-s2", "--s2_tile", dest="s2_tile_shp", help="path to s2 tiles shapefile", required=False)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    if args.in_filter:
        shp_filter= args.in_filter
    else:
        shp_filter = None
    in_shapefiles, in_zip_shapefiles = get_shp_list(args.rpg_dir, shp_filter)
    print("merge shapefiles :")
    for shp in in_shapefiles + in_zip_shapefiles:
        print(shp)

    # init sqlite db with first shapefile in list
    if len(in_shapefiles) != 0:
        first_shp = in_shapefiles[0]
        in_shapefiles = in_shapefiles[1:]
    else:
        first_shp = os.path.join("/vsizip", in_zip_shapefiles[0])
        in_zip_shapefiles = in_zip_shapefiles[1:]

    print("create sqlite and import first shapefile : {0}".format(first_shp))
    if not os.path.exists( args.out_name):
        sqlite_init_args = [
            "ogr2ogr", "-f", "SQLite", "-dsco", "SPATIALITE=YES", args.out_name, first_shp, "-nlt", "PROMOTE_TO_MULTI",
            "-nln", args.out_table_name, "-lco", "SPATIAL_INDEX=NO"]
    else:
        sqlite_init_args = [
           "ogr2ogr", "-update", args.out_name, first_shp, "-nln", args.out_table_name, "-nlt", "PROMOTE_TO_MULTI",
            "-lco", "SPATIAL_INDEX=NO"]

    subprocess.call(sqlite_init_args)

    # append all other shapefile
    for shp in in_shapefiles:
        print("append shapefile : {0}".format(shp))
        sqlite_append_args = [
           "ogr2ogr", "-append", args.out_name, shp, "-nln", args.out_table_name, "-nlt", "PROMOTE_TO_MULTI"]
        subprocess.call(sqlite_append_args)

    for zip_shp in in_zip_shapefiles:
        print("append shapefile : {0}".format(zip_shp))
        shp = os.path.join("/vsizip", zip_shp)
        sqlite_append_args = [
           "ogr2ogr", "-append", args.out_name, shp, "-nln", args.out_table_name, "-nlt", "PROMOTE_TO_MULTI"]
        subprocess.call(sqlite_append_args)
    
    # create spatial index
    print("create sqlite spatial index on table {0}".format(args.out_table_name))
    sqlite_index_args = [
        "ogrinfo", args.out_name, "-sql", 
        "SELECT CreateSpatialIndex('{0}','GEOMETRY')".format(args.out_table_name)]
    subprocess.call(sqlite_index_args)
    
    if args.s2_tile_shp:
        sqlite_append_args = [
           "ogr2ogr", "-append", args.out_name, args.s2_tile_shp, "-nln", "s2_tiles"]
        subprocess.call(sqlite_append_args)
        # sqlite_index_args = [
        #    "ogrinfo", args.out_name, "-sql",
        #    "SELECT CreateSpatialIndex('{0}','GEOMETRY')".format("s2_tiles")]
        # subprocess.call(sqlite_index_args)
