#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging
import sys
import sqlite3
import random
import pandas as pd
import numpy as np

from tqdm import tqdm


def random_sampling_df(in_df, nb_sample=None, ratio=0.20):
    """
    """
    if not nb_sample and not ratio:
        raise ValueError("one of nb_sample or ratio must be not null")
    if nb_sample:
        idx = random.sample(list(in_df.index.values), nb_sample)
    else:
        pop_size = len(in_df)
        idx = random.sample(list(in_df.index.values), int(pop_size*1.0*ratio))

    out_sample = in_df.ix[idx]
    out_sample.index.name = in_df.index.name
    return out_sample


def kfold_sampling_df(in_df, fold_cols, num_fold=5):
    """

    :param in_df:
    :param fold_cols:
    :param num_fold:
    :return:
    """
    idx = in_df.index.values.copy()
    np.random.shuffle(idx)
    kfold_idx = np.array_split(idx, num_fold)
    np.random.shuffle(kfold_idx)
    out_sample = in_df.ix[idx]
    i = 1
    for fold in kfold_idx:
        out_sample.ix[fold, fold_cols] = f"{i}"
        i += 1

    out_sample.index.name = in_df.index.name
    return out_sample


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="sample a sqlite table based on a code/group column and a classif columns")

    parser.add_argument("-i", "--in_db", dest="in_db", help="spatialite_db", required=True)
    parser.add_argument("-t", "--in_table", dest="in_table", help="table to process", required=True)
    parser.add_argument(
        "-id", "--id_field", dest="id_field",
        help="name of the input field containing unique id to use",
        required=True)
    parser.add_argument(
        "-gf", "--code_field", dest="code_field", help="name of the output filed containing the number of pixel",
        required=True)
    parser.add_argument(
        "-cf", "--classif_field", dest="classif_field",
        help="name of the input filed containing the use of feature in classif: NONE|TRAIN|TEST|VALIDATION|NUM_FOLD",
        required=True)
    parser.add_argument(
        "-wc", "--where_clause", dest="where_clause", help="where clause for selection of feature to sample",
        required=False)
    parser.add_argument(
        "-s", "--strategy", dest="strategy", help="sample strategy: rf_2018, sen4cap, kfold",
        required=True)
    parser.add_argument(
        "-nf", "--num_fold", dest="num_fold", help="number of fold for k fold strategy", type=int,
        required=False)
    parser.add_argument(
        "-m", "--min_size", dest="min_size", help="min class size for sample", type=int,
        required=False)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    min_size = 0
    if args.min_size:
        min_size = args.min_size

    # 1 test spatialite connection
    conn = sqlite3.connect(args.in_db)
    conn.enable_load_extension(True)
    try:
        conn.load_extension("mod_spatialite")
    except:
        print("load_extension(mod_spatialite) failed, Error: ", sys.exc_info()[1], "occured.")
        print('\n\nTry loading using the name of the dll (spatialite.dll)')
        conn.load_extension("spatialite")

    uid_field = args.id_field
    code_field = args.code_field
    classif_field = args.classif_field

    cur = conn.cursor()

    query_type = f"SELECT type FROM pragma_table_info('{args.in_table}') WHERE name = '{uid_field}'"
    cur.execute(query_type)
    rows = cur.fetchall()
    print(f"rows {rows}")
    type_id = rows[0][0]
    print(f"type_id {type_id}")

    # print("query data")
    select_query = f"SELECT {uid_field}, {code_field}, {classif_field} FROM {args.in_table}"
    if args.where_clause:
        select_query += f" {args.where_clause}"
    # print(f"select_query \n{select_query}")
    curr_row = cur.execute(select_query)
    rows = cur.fetchall()
    cols = [column[0] for column in curr_row.description]

    # load data from sqlite table into a pandas dataframe for easily process group
    print("load data")
    uid_array = []
    code_array = []
    classif_array = []
    for row in rows:
        uid_array.append(row[0])
        code_array.append(row[1])
        classif_array.append(row[2])
    lpis_df = pd.DataFrame(
        data={uid_field: uid_array,
              code_field: code_array,
              classif_field: classif_array})
    lpis_df.set_index(uid_field, inplace=True)
    # print(lpis_df.head())

    # sample dataframe

    if args.strategy in ["rf_2018", "sen4cap"]:

        if args.strategy == "rf_2018":
            # sampling parameter
            big_threshold = 5000
            ratio_sample_big = None
            nb_sample_big = 2500
            ratio_sample_small = 0.5
            nb_sample_small = None

        if args.strategy == "sen4cap":
            # sampling parameter
            big_threshold = 4000
            nb_sample_big = None
            nb_sample_small = None
            ratio_sample_big = 0.25
            ratio_sample_small = 0.75

        print("sample data")

        gb = lpis_df.groupby([code_field])  # group input element by crop code

        df_pop = gb.filter(lambda x: len(x.index) >= big_threshold)  # filter group by size, keep ony big one
        df_sample = df_pop.groupby([code_field]).apply(
            lambda x: random_sampling_df(x, nb_sample=nb_sample_big, ratio=ratio_sample_big))  # sample big group
        df_sample.reset_index(level=0, drop=True, inplace=True)

        # filter group by size, keep ony small one
        out_df_small = gb.filter(lambda x: (big_threshold > len(x.index) >= min_size))
        df_sample_small = out_df_small.groupby([code_field]).apply(
            lambda x: random_sampling_df(x, nb_sample=nb_sample_small, ratio=ratio_sample_small))  # sample big group
        df_sample_small.reset_index(level=0, drop=True, inplace=True)
        out_df = pd.concat((df_sample_small, df_sample))  # concat small group and big group sample
        train_idx = out_df.index

        # update classif column
        # conn.execute('BEGIN TRANSACTION')
        # count = 0
        # for train_id in tqdm(train_idx):
        #     count += 1
        #
        #     if count % 10000 == 0:
        #         # print(count)
        #         conn.execute('COMMIT')
        #         conn.execute('BEGIN TRANSACTION')
        #
        #     if type_id == 'VARCHAR':
        #         query_update = f"UPDATE {args.in_table} SET {classif_field} = 'TRAIN' WHERE {uid_field} = '{train_id}';"
        #     else:
        #         query_update = f"UPDATE {args.in_table} SET {classif_field} = 'TRAIN' WHERE {uid_field} = {train_id};"
        #     conn.execute(query_update)
        #
        # conn.execute('COMMIT')
        # conn.commit()

        print("create sample table")

        sample_table = f"rpg_sample_sql_{args.in_table}"
        query_drop_table = f"DROP TABLE IF EXISTS {sample_table};"
        conn.execute(query_drop_table)
        query_create_table = f"CREATE TABLE {sample_table}( {uid_field} {type_id} NOT NULL PRIMARY KEY);"
        conn.execute(query_create_table)

        print("insert into sample table")
        conn.execute('BEGIN TRANSACTION')
        count = 0
        for train_id in tqdm(train_idx):
            count += 1
            if count % 10000 == 0:
                # print(count)
                conn.execute('COMMIT')
                conn.execute('BEGIN TRANSACTION')

            query_insert = f"INSERT INTO {sample_table} ({uid_field}) VALUES (?)"
            conn.execute(query_insert, (train_id,))

        conn.execute('COMMIT')
        conn.commit()

        print("create sample table index")
        query_insert_index = f"CREATE INDEX {sample_table}_index_id ON {sample_table}({uid_field});"
        conn.execute(query_insert_index)
        print("update clasif field ")
        query_update = f"UPDATE {args.in_table} SET {classif_field} = 'TRAIN' "
        query_update += f" WHERE {uid_field} IN (SELECT {uid_field} FROM {sample_table});"

        query_udpate_2 = f" UPDATE {args.in_table} "
        query_udpate_2 +=f"SET {classif_field} = 'TRAIN' "
        query_udpate_2 += f"WHERE EXISTS(SELECT {uid_field}  "
        query_udpate_2 += f"             FROM {sample_table} "
        query_udpate_2 += f"             WHERE {uid_field} = {args.in_table}.{uid_field}); "

        conn.execute(query_udpate_2)
        print("drop sample table ")
        conn.execute(query_drop_table)
        conn.commit()

    if args.strategy in ["kfold"]:

        if not args.num_fold:
            raise ValueError("param num_fold is required for kfold strategy")

        gb = lpis_df.groupby([code_field])  # group input element by crop code
        if min_size < args.num_fold:
            min_size = args.num_fold

        df_pop = gb.filter(lambda x: len(x.index) >= min_size)  # filter group by size, keep ony big one
        out_df = df_pop.groupby([code_field]).apply(
            lambda x: kfold_sampling_df(x, num_fold=args.num_fold, fold_cols=classif_field))  # sample big group
        out_df.reset_index(level=0, drop=True, inplace=True)

        # update classif column

        train_idx = out_df.index
        conn.execute('BEGIN TRANSACTION')
        count = 0
        for train_id in tqdm(train_idx):
            count += 1
            fold_value = out_df.loc[train_id][classif_field]
            if type_id == 'VARCHAR':
                query_update = f"UPDATE {args.in_table} SET {classif_field} = '{fold_value}' WHERE {uid_field} = '{train_id}';"
            else:
                query_update = f"UPDATE {args.in_table} SET {classif_field} = '{fold_value}' WHERE {uid_field} = {train_id};"

            if count % 6000 == 0:
                # print(count)
                # print(f"fold_value {fold_value}")
                # print(query_update)
                conn.execute('COMMIT')
                conn.execute('BEGIN TRANSACTION')

            conn.execute(query_update)

        conn.execute('COMMIT')
        conn.commit()

    cur.close()
