#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"

echo " drop and create lpis_crop tables "

spatialite $DB <<END_SQL
SELECT DropGeoTable('lpis_crop');
SELECT DropGeoTable('lpis_crop_mp');

CREATE TABLE lpis_crop_mp (
  id TEXT NOT NULL PRIMARY KEY,
  code_cultu TEXT NOT NULL,
  pacage TEXT NOT NULL,
  CODE9 TEXT NOT NULL,
  N2019 INTEGER,
  CODE9_EN TEXT,
  npix10b5_ini INTEGER,
  npix20b10_ini INTEGER,
  compactness_ini REAL,
  qa_ratio_ini REAL);
SELECT AddGeometryColumn('lpis_crop_mp', 'GEOMETRY',  (SELECT srid FROM geometry_columns WHERE f_table_name = 'rpg_ini') , 'MULTIPOLYGON', 'XY');
END_SQL

echo " drop and create lpis_nocrop and insert data to nocrop "

spatialite $DB <<END_SQL
SELECT DropGeoTable('lpis_nocrop');
CREATE TABLE lpis_nocrop (
  id TEXT NOT NULL PRIMARY KEY,
  type TEXT NOT NULL);
SELECT AddGeometryColumn('lpis_nocrop', 'GEOMETRY',  (SELECT srid FROM geometry_columns WHERE f_table_name = 'rpg_ini') , 'MULTIPOLYGON', 'XY');
INSERT INTO lpis_nocrop SELECT numero_sna, type, CastToMultiPolygon(ST_Buffer(GEOMETRY, 1)) FROM vg_ini;
INSERT INTO lpis_nocrop SELECT numero_sna, type, CastToMultiPolygon(ST_Buffer(GEOMETRY, 1)) FROM ea_ini;
INSERT INTO lpis_nocrop SELECT numero_sna, type, CastToMultiPolygon(ST_Buffer(GEOMETRY, 1)) FROM at_ini;
INSERT INTO lpis_nocrop SELECT numero_sna, type, CastToMultiPolygon(ST_Buffer(GEOMETRY, 4)) FROM v1_ini;
SELECT CreateSpatialIndex('lpis_nocrop','GEOMETRY');
CREATE INDEX lpis_nocrop_index_id ON lpis_nocrop(id);
END_SQL

echo " clean up geom "

spatialite $DB <<END_SQL
UPDATE rpg_ini SET GEOMETRY = CastToMultiPolygon(St_Buffer(GEOMETRY, 0))  WHERE St_IsValid(GEOMETRY) <> 1;
UPDATE lpis_nocrop SET GEOMETRY = CastToMultiPolygon(St_Buffer(GEOMETRY, 0))  WHERE St_IsValid(GEOMETRY) <> 1;
END_SQL

# echo" drop and create lpis_nocrop and insert data to nocrop "
# spatialite $DB <<END_SQL
# SELECT DropGeoTable('lpis_nocrop');
# CREATE TABLE lpis_nocrop (
  # id TEXT NOT NULL PRIMARY KEY,
  # type TEXT NOT NULL);
# SELECT AddGeometryColumn('lpis_nocrop', 'GEOMETRY',  (SELECT srid FROM geometry_columns WHERE f_table_name = 'rpg_ini') , 'MULTIPOLYGON', 'XY');
# INSERT INTO lpis_nocrop SELECT numero_sna, type, CastToMultiPolygon(ST_Simplify(ST_Buffer(GEOMETRY, 1), 0.5)) FROM vg_ini;
# INSERT INTO lpis_nocrop SELECT numero_sna, type, CastToMultiPolygon(ST_Simplify(ST_Buffer(GEOMETRY, 1), 0.5)) FROM ea_ini;
# INSERT INTO lpis_nocrop SELECT numero_sna, type, CastToMultiPolygon(ST_Simplify(ST_Buffer(GEOMETRY, 1), 0.5)) FROM at_ini;
# SELECT CreateSpatialIndex('lpis_nocrop','GEOMETRY');
# CREATE INDEX lpis_nocrop_index_id ON lpis_nocrop(id);
# END_SQL


echo " spatial join rpg_ini and nocrop "

spatialite $DB <<END_SQL
INSERT INTO lpis_crop_mp 
SELECT 
    lpis_erase.id,
    lpis_erase.code_cultu,
    lpis_erase.pacage,
    lpis_erase.CODE9,
    lpis_erase.N2019,
    lpis_erase.CODE9,
    lpis_erase.npix10b5,
    lpis_erase.npix20b10,
    lpis_erase.compactness,
    lpis_erase.qa_ratio,
    CASE St_IsValid(lpis_erase.GEOMETRY) WHEN 1 THEN CastToMultiPolygon(lpis_erase.GEOMETRY) ELSE geom_ini END
FROM (
    SELECT
        lpis.id,
        lpis.code_cultu,
        lpis.pacage,
        lpis.CODE9,
        lpis.N2019,
        lpis.CODE9,
        lpis.npix10b5,
        lpis.npix20b10,
        lpis.compactness,
        lpis.qa_ratio,
        ifnull(ST_Simplify(ST_Buffer(ST_Difference(lpis.GEOMETRY, ST_Union(sna.GEOMETRY)), 0), 0.5),lpis.GEOMETRY)  AS GEOMETRY,
        lpis.GEOMETRY as geom_ini
    FROM rpg_ini lpis 
    LEFT JOIN lpis_nocrop sna  ON ST_Intersects(lpis.GEOMETRY, sna.GEOMETRY) 
              AND sna.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'lpis_nocrop' AND search_frame = lpis.GEOMETRY)
    WHERE st_isvalid(lpis.GEOMETRY) 
    GROUP BY lpis.id
) AS lpis_erase
WHERE ST_GeometryType(lpis_erase.GEOMETRY) in ("POLYGON", "MULTIPOLYGON") 
     AND ST_Srid(lpis_erase.GEOMETRY) = (SELECT srid FROM geometry_columns WHERE f_table_name = 'rpg_ini');

SELECT CreateSpatialIndex('lpis_crop_mp','GEOMETRY');
CREATE INDEX lpis_crop_mp_index_id ON lpis_crop_mp(id);
END_SQL

echo " multi part to single part "
spatialite $DB <<END_SQL
SELECT ElementaryGeometries('lpis_crop_mp', 'GEOMETRY', 'lpis_crop','id_s','id_m');
SELECT CreateSpatialIndex('lpis_crop','GEOMETRY');
CREATE INDEX lpis_crop_index_id ON lpis_crop(id_s);
CREATE INDEX lpis_crop_index_id_multi ON lpis_crop(id);

END_SQL

spatialite $DB <<END_SQL
SELECT DropGeoTable('lpis_crop_mp');
END_SQL


