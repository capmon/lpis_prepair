#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"

spatialite $DB <<END_SQL
ALTER TABLE rpg_foi_result ADD classif_rf VARCHAR;
ALTER TABLE rpg_foi_result ADD classif_sen4cap VARCHAR;
ALTER TABLE rpg_foi_result ADD kfold5 VARCHAR;

UPDATE rpg_foi_result
SET classif_rf = "VALIDATION",
    classif_sen4cap = "VALIDATION",
    kfold5 = "VALIDATION"
WHERE id IN (
SELECT rpg_foi_result.id
FROM rpg_foi_result INNER JOIN (SELECT * FROM rpg_ini WHERE classif_rf == "VALIDATION") as valid_ini 
   ON ST_Intersects(rpg_foi_result.GEOMETRY, ST_Buffer(valid_ini.GEOMETRY, -1)) 
   AND rpg_foi_result.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'rpg_foi_result' AND search_frame = valid_ini.GEOMETRY)
);

ALTER TABLE lpis_crop_foi_result ADD classif_rf VARCHAR;
ALTER TABLE lpis_crop_foi_result ADD classif_sen4cap VARCHAR;
ALTER TABLE lpis_crop_foi_result ADD kfold5 VARCHAR;

UPDATE lpis_crop_foi_result
SET classif_rf = "VALIDATION",
    classif_sen4cap = "VALIDATION",
    kfold5 = "VALIDATION"
WHERE id_s IN (
SELECT lpis_crop_foi_result.id_s
FROM lpis_crop_foi_result INNER JOIN (SELECT * FROM rpg_ini WHERE classif_rf == "VALIDATION") as valid_ini 
   ON ST_Intersects(lpis_crop_foi_result.GEOMETRY, ST_Buffer(valid_ini.GEOMETRY, -1)) 
   AND lpis_crop_foi_result.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'lpis_crop_foi_result' AND search_frame = valid_ini.GEOMETRY)
);

END_SQL



