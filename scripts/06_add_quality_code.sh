#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"

spatialite $DB <<END_SQL
UPDATE rpg_ini SET npix10b5 = 0 WHERE npix10b5 IS NULL;
UPDATE rpg_ini SET npix10b5 = cast(FLOOR(ST_Area(GEOMETRY)/100) as int) WHERE (npix10b5*100) >= ST_Area(GEOMETRY);
ALTER TABLE rpg_ini ADD compactness REAL;
UPDATE rpg_ini SET compactness = (4*PI()*ST_Area(GEOMETRY))/(ST_Perimeter(GEOMETRY)*ST_Perimeter(GEOMETRY));
ALTER TABLE rpg_ini ADD qa_ratio REAL;
UPDATE rpg_ini SET qa_ratio = (1 - (npix10b5*100)/ST_Area(GEOMETRY));
END_SQL

