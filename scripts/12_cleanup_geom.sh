#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
db_filename=${DB##*/}
db_base=${db_filename%.*} 
S2_TILE=$2
echo "S2_TILE ${S2_TILE}"

echo "keep only one geom for duplicate id"
spatialite $DB <<END_SQL

DELETE FROM rpg_ini
WHERE ogc_fid IN (
SELECT ogc_fid
FROM rpg_ini JOIN (
    SELECT id, MAX(ST_Area(GEOMETRY)) as surf_mmax
    FROM rpg_ini 
    GROUP BY id HAVING COUNT( *) > 1 ) as duplicate 
    ON rpg_ini.id = duplicate.id AND ST_Area(GEOMETRY) != duplicate.surf_mmax );
    
END_SQL

echo "clean rpg_ini"
spatialite $DB <<END_SQL
SELECT DropGeoTable('rpg_ini_clean');

CREATE TABLE rpg_ini_clean (
  id TEXT NOT NULL PRIMARY KEY,
  pacage TEXT NOT NULL,
  num_ilot TEXT NOT NULL,
  num_parcel TEXT NOT NULL,
  code_cultu TEXT NOT NULL,
  surf_adm REAL,
  CODE9 TEXT NOT NULL,
  CODE9_EN TEXT,
  N2019 INTEGER,
  npix10b5 INTEGER,
  npix20b10 INTEGER,
  compactness REAL,
  qa_ratio REAL,
  classif_rf TEXT,
  classif_sen4cap TEXT,
  kfold5 TEXT
  );
SELECT AddGeometryColumn('rpg_ini_clean', 'GEOMETRY',  (SELECT srid FROM geometry_columns WHERE f_table_name = 'rpg_ini') , 'MULTIPOLYGON', 'XY');

INSERT INTO rpg_ini_clean 
SELECT 
    rpg_ini.id,
    rpg_ini.pacage,
    rpg_ini.num_ilot,
    rpg_ini.num_parcel,
    rpg_ini.code_cultu,
    rpg_ini.surf_adm,
    rpg_ini.CODE9,
    rpg_ini.CODE9_EN,
    rpg_ini.N2019,
    rpg_ini.npix10b5,
    rpg_ini.npix20b10,
    rpg_ini.compactness,
    rpg_ini.qa_ratio,
    rpg_ini.classif_rf,
    rpg_ini.classif_sen4cap,
    rpg_ini.kfold5,
    CastToMultiPolygon(ST_Simplify(ST_Intersection(rpg_ini.GEOMETRY, ST_Buffer(ST_Buffer(rpg_ini.GEOMETRY, -0.5), 0.7)), 0.5) )
FROM rpg_ini
WHERE ST_IsValid(ST_Buffer(rpg_ini.GEOMETRY, -0.5)) == 1;

SELECT CreateSpatialIndex('rpg_ini_clean','GEOMETRY');
CREATE INDEX rpg_ini_clean_index_id ON rpg_ini_clean(id);
END_SQL

echo "clean lpis_crop"
spatialite $DB <<END_SQL
SELECT DropGeoTable('lpis_crop_clean');
CREATE TABLE lpis_crop_clean (
  id_s TEXT NOT NULL PRIMARY KEY,
  id TEXT NOT NULL,
  pacage TEXT NOT NULL,
  code_cultu TEXT NOT NULL,
  CODE9 TEXT NOT NULL,
  CODE9_EN TEXT,
  N2019 INTEGER,
  npix10b5_ini INTEGER,
  npix20b10_ini INTEGER,
  compactness_ini REAL,
  qa_ratio_ini REAL,
  npix10b5 INTEGER,
  npix20b10 INTEGER,
  compactness REAL,
  qa_ratio REAL,
  classif_rf TEXT,
  classif_sen4cap TEXT,
  kfold5 TEXT
  );
SELECT AddGeometryColumn('lpis_crop_clean', 'GEOMETRY',  (SELECT srid FROM geometry_columns WHERE f_table_name = 'rpg_ini') , 'MULTIPOLYGON', 'XY');

INSERT INTO lpis_crop_clean 
SELECT 
    lpis_crop.id_s,
    lpis_crop.id,
    lpis_crop.pacage,
    lpis_crop.code_cultu,
    lpis_crop.CODE9,
    lpis_crop.CODE9_EN,
    lpis_crop.N2019,
    lpis_crop.npix10b5_ini,
    lpis_crop.npix20b10_ini,
    lpis_crop.compactness_ini,
    lpis_crop.qa_ratio_ini,
    lpis_crop.npix10b5,
    lpis_crop.npix20b10,
    lpis_crop.compactness,
    lpis_crop.qa_ratio,
    lpis_crop.classif_rf,
    lpis_crop.classif_sen4cap,
    lpis_crop.kfold5,
    CastToMultiPolygon(ST_Simplify(ST_Intersection(lpis_crop.GEOMETRY, ST_Buffer(ST_Buffer(lpis_crop.GEOMETRY, -0.5), 0.7)), 0.5) )
FROM lpis_crop
WHERE ST_IsValid(ST_Buffer(lpis_crop.GEOMETRY, -0.5)) == 1 ;

SELECT CreateSpatialIndex('lpis_crop_clean','GEOMETRY');
CREATE INDEX lpis_crop_clean_index_id ON lpis_crop_clean(id);

END_SQL
