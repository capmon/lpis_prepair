#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
db_filename=${DB##*/}
db_base=${db_filename%.*} 
S2_TILE=$2
echo "S2_TILE ${S2_TILE}"

spatialite $DB <<END_SQL
SELECT DropGeoTable('rpg_ini_train_iota2_mp');
SELECT DropGeoTable('rpg_ini_train_iota2');

CREATE TABLE rpg_ini_train_iota2_mp (
  id TEXT NOT NULL PRIMARY KEY,
  code_cultu TEXT NOT NULL,
  pacage TEXT NOT NULL,
  num_ilot TEXT NOT NULL,
  num_parcel TEXT NOT NULL,
  code9 TEXT NOT NULL,
  n2019 INTEGER,
  code9_en TEXT,
  npx10b5 INTEGER,
  npx20b10 INTEGER,
  cmptcness REAL,
  qa_ratio REAL,
  ciota INTEGER);
SELECT AddGeometryColumn('rpg_ini_train_iota2_mp', 'GEOMETRY',  (SELECT srid FROM geometry_columns WHERE f_table_name = 'rpg_ini') , 'MULTIPOLYGON', 'XY');

INSERT INTO rpg_ini_train_iota2_mp 
SELECT 
    rpg_ini.id,
    rpg_ini.code_cultu,
    rpg_ini.pacage,
    rpg_ini.num_ilot,
    rpg_ini.num_parcel,
    rpg_ini.CODE9 as code9,
    rpg_ini.N2019 as n2019,
    rpg_ini.CODE9_EN as code9_en,
    rpg_ini.npix10b5 as npx10b5,
    rpg_ini.npix20b10 as npx20b10,
    rpg_ini.compactness as cmptcness,
    rpg_ini.qa_ratio,
    rpg_ini.N2019 as ciota,
    CastToMultiPolygon(ST_Buffer(rpg_ini.GEOMETRY, -5))
FROM rpg_ini
WHERE classif_rf = 'TRAIN' AND ST_IsValid(ST_Buffer(rpg_ini.GEOMETRY, -5));

SELECT CreateSpatialIndex('rpg_ini_train_iota2_mp','GEOMETRY');
CREATE INDEX rpg_ini_train_iota2_mp_index_id ON rpg_ini_train_iota2_mp(id);

DELETE FROM rpg_ini_train_iota2_mp as iota2_mp
WHERE iota2_mp.id NOT IN (
   SELECT min(s2.id) as id_to_keep
   FROM  rpg_ini_train_iota2_mp as s1, 
         rpg_ini_train_iota2_mp as s2
   WHERE s2.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'rpg_ini_train_iota2_mp' AND search_frame = s1.GEOMETRY)
         AND st_equals(s1.GEOMETRY, s2.GEOMETRY)
   GROUP BY s1.id
); 

SELECT ElementaryGeometries('rpg_ini_train_iota2_mp', 'GEOMETRY', 'rpg_ini_train_iota2','id_s','id_m');

DELETE FROM rpg_ini_train_iota2 
WHERE St_Area(GEOMETRY) < 400; 

SELECT CreateSpatialIndex('rpg_ini_train_iota2','GEOMETRY');
CREATE INDEX rpg_ini_train_iota2_index_id ON rpg_ini_train_iota2(id);

END_SQL


QUERY_RPG="SELECT * FROM rpg_ini_train_iota2 WHERE ST_IsValid(GEOMETRY)"
echo "export to ${S2_TILE}/${db_base}_rpg_ini_train_iota2.shp"

ogr2ogr -f "ESRI Shapefile" "${S2_TILE}/${db_base}_rpg_ini_train_iota2.shp" ${DB} -dialect sqlite -sql "$QUERY_RPG"

spatialite $DB <<END_SQL
SELECT DropGeoTable('lpis_crop_foi_result_iota2_mp');
SELECT DropGeoTable('lpis_crop_foi_result_iota2');

CREATE TABLE lpis_crop_foi_result_iota2_mp (
  id_s TEXT NOT NULL PRIMARY KEY,
  code_cultu TEXT NOT NULL,
  foi INTEGER,
  foi_type TEXT,
  code9 TEXT NOT NULL,
  n2019 INTEGER,
  code9_en TEXT,
  npx10b5 INTEGER,
  npx20b10 INTEGER,
  cmptcness REAL,
  qa_ratio REAL,
  ciota INTEGER);
SELECT AddGeometryColumn('lpis_crop_foi_result_iota2_mp', 'GEOMETRY',  (SELECT srid FROM geometry_columns WHERE f_table_name = 'rpg_ini') , 'MULTIPOLYGON', 'XY');

INSERT INTO lpis_crop_foi_result_iota2_mp 
SELECT 
    lpis_crop_foi_result.id_s,
    lpis_crop_foi_result.code_cultu,
    lpis_crop_foi_result.FOI as foi,
    lpis_crop_foi_result.foi_type as foi_type,
    lpis_crop_foi_result.CODE9 as code9,
    lpis_crop_foi_result.N2019 as n2019,
    lpis_crop_foi_result.CODE9_EN as code9_en,
    lpis_crop_foi_result.npix10b5 as npx10b5,
    lpis_crop_foi_result.npix20b10 as npx20b10,
    lpis_crop_foi_result.compactness as cmptcness,
    lpis_crop_foi_result.qa_ratio,
    lpis_crop_foi_result.N2019 as ciota,
    CastToMultiPolygon(ST_Buffer(lpis_crop_foi_result.GEOMETRY, -5))
FROM lpis_crop_foi_result
WHERE classif_rf = 'TRAIN' AND ST_IsValid(ST_Buffer(lpis_crop_foi_result.GEOMETRY, -5));

SELECT CreateSpatialIndex('lpis_crop_foi_result_iota2_mp','GEOMETRY');
CREATE INDEX rlpis_crop_foi_result_iota2_mp_index_id ON lpis_crop_foi_result_iota2_mp(id_s);

DELETE FROM lpis_crop_foi_result_iota2_mp as iota2_mp
WHERE iota2_mp.id_s NOT IN (
   SELECT min(s2.id_s) as id_to_keep
   FROM  lpis_crop_foi_result_iota2_mp as s1, 
         lpis_crop_foi_result_iota2_mp as s2
   WHERE s2.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'lpis_crop_foi_result_iota2_mp' AND search_frame = s1.GEOMETRY)
         AND st_equals(s1.GEOMETRY, s2.GEOMETRY)
   GROUP BY s1.id_s
); 

SELECT ElementaryGeometries('lpis_crop_foi_result_iota2_mp', 'GEOMETRY', 'lpis_crop_foi_result_iota2','id_ss','id_foi');

DELETE FROM lpis_crop_foi_result_iota2 
WHERE St_Area(GEOMETRY) < 400; 

SELECT CreateSpatialIndex('lpis_crop_foi_result_iota2','GEOMETRY');
CREATE INDEX lpis_crop_foi_result_iota2_index_id ON lpis_crop_foi_result_iota2(id_s);

END_SQL


QUERY_RPG="SELECT * FROM lpis_crop_foi_result_iota2 WHERE ST_IsValid(GEOMETRY)"
echo "export to ${S2_TILE}/${db_base}_lpis_crop_foi_iota2.shp"
ogr2ogr -f "ESRI Shapefile" "${S2_TILE}/${db_base}_lpis_crop_foi_iota2.shp" ${DB} -dialect sqlite -sql "$QUERY_RPG"
