#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
S2_TILE=$2

db_filename=${DB##*/}
db_base=${db_filename%.*} 


db_filename=${DB##*/}
db_base=${db_filename%.*} 
S2_TILE=$2
echo "S2_TILE ${S2_TILE}"

spatialite $DB <<END_SQL
SELECT DropGeoTable('rpg_foi_result');
SELECT DropGeoTable('lpis_crop_foi_result');

END_SQL

FOI_INI_DB="${S2_TILE}/FOI_RPG_INI/${db_base}_rpg_ini_all_foi.sqlite"
PREFIX_RPG_INI="${db_base}_rpg_ini_all"
PREFIX_RPG_INI_LOWER="$(echo $PREFIX_INI | tr '[A-Z]' '[a-z]')"

ogr2ogr -update ${DB} ${S2_TILE}/${PREFIX_RPG_INI}_foi_result.shp -nlt PROMOTE_TO_MULTI -nlt MULTIPOLYGON -nln rpg_foi_result -lco SPATIAL_INDEX=YES

PREFIX_CROP_INI="${db_base}_lpis_crop_all"
PREFIX_CROP_INI_LOWER="$(echo $PREFIX_INI | tr '[A-Z]' '[a-z]')"
ogr2ogr -update ${DB} ${S2_TILE}/${PREFIX_CROP_INI}_foi_result.shp -nlt PROMOTE_TO_MULTI -nlt MULTIPOLYGON -nln lpis_crop_foi_result -lco SPATIAL_INDEX=YES

spatialite $DB <<END_SQL
CREATE INDEX rpg_foi_result_index_id ON rpg_foi_result(id);
CREATE INDEX lpis_crop_foi_result_result_index_id ON lpis_crop_foi_result(id_s);

END_SQL
