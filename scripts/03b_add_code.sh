#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"

sqlite3 $DB <<END_SQL
UPDATE rpg_ini 
SET (CODE9, N2019, CODE9_EN) = (
  SELECT cultu_73.CODE9, CAST(cultu_73.N2019 as integer), cultu_73.CODE9_EN
  FROM rpg_ini LEFT JOIN (
     SELECT lut_cultu_73.code_cultu code_cultu, lut_cultu_73.CODE9 CODE9, nomenclature_73.N2019  N2019, nomenclature_73.CODE9_EN  CODE9_EN
       FROM lut_cultu_73 LEFT JOIN nomenclature_73 ON lut_cultu_73.CODE9 = nomenclature_73.CODE9
     ) AS cultu_73 
  
  ON rpg_ini.code_cultu = cultu_73.code_cultu);
END_SQL
