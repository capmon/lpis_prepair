#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
db_filename=${DB##*/}
db_base=${db_filename%.*} 
S2_TILE=$2
echo "S2_TILE ${S2_TILE}"

LPIS_CROP_FIELD="id_s, id, pacage ,code_cultu, CODE9, CODE9_EN, N2019,"
LPIS_CROP_FIELD=${LPIS_CROP_FIELD}"npix10b5_ini as npx10b5_i, npix20b10_ini as npx20b10_i, compactness_ini as cmptcnes_i, qa_ratio_ini as qa_ratio_i, "
LPIS_CROP_FIELD=${LPIS_CROP_FIELD}"npix10b5 as npx10b5, npix20b10 as npx20b10, compactness as cmptcness, qa_ratio, classif_rf as rf_t, classif_sen4cap as sen4cap_t, kfold5 as kflod_t,"
LPIS_CROP_FIELD=${LPIS_CROP_FIELD}" ST_Simplify(GEOMETRY, 1) AS GEOMETRY, ST_Area(GEOMETRY) as surf_adm "

QUERY_LPIS_CROP="SELECT ${LPIS_CROP_FIELD} FROM lpis_crop WHERE ST_IsValid(GEOMETRY)"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${db_base}_lpis_crop_all_simplify.shp ${DB} -dialect sqlite -sql "$QUERY_LPIS_CROP"
QUERY_LPIS_CROP="SELECT ${LPIS_CROP_FIELD} FROM lpis_crop WHERE rf_t = 'VALIDATION' AND ST_IsValid(GEOMETRY)"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${db_base}_lpis_crop_val_simplify.shp ${DB} -dialect sqlite -sql "$QUERY_LPIS_CROP"
QUERY_LPIS_CROP="SELECT ${LPIS_CROP_FIELD} FROM lpis_crop WHERE rf_t = 'TRAIN' AND ST_IsValid(GEOMETRY)"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${db_base}_lpis_crop_train_rf_simplify.shp ${DB} -dialect sqlite -sql "$QUERY_LPIS_CROP"
