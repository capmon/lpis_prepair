#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

for file in ./$1/*.shp; do
   filename=${file##*/}
   base=${filename%.*} 
   out_name="./$1/$base.cpg"
    if [ -f "$out_name" ]; then
        echo $out_name
    else
	echo "create $out_name"
	echo "ISO-8859-1">$out_name
    fi
done;

