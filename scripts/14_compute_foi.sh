#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
db_filename=${DB##*/}
db_base=${db_filename%.*} 
S2_TILE=$2
echo "S2_TILE $S2_TILE"
CODE_DIR=$3
echo "dir to rpg/lpis preprocess code = ${CODE_DIR}"

mkdir -p ${S2_TILE}/FOI_RPG_INI
python ${CODE_DIR}/script/rpg_lpis_foi.py --in_lpis ${S2_TILE}/${db_base}_rpg_ini_all.shp --work_dir ${S2_TILE}/FOI_RPG_INI --threshold_ha 0.5 --field_code CODE9 --field_id id --exclude_codes SURF_TNEX BORDURE
mkdir -p ${S2_TILE}/FOI_LPIS_CROP
python ${CODE_DIR}/script/rpg_lpis_foi.py --in_lpis ${S2_TILE}/${db_base}_lpis_crop_all.shp --work_dir ${S2_TILE}/FOI_LPIS_CROP --threshold_ha 0.5 --field_code CODE9 --field_id id_s --exclude_codes SURF_TNEX BORDURE




