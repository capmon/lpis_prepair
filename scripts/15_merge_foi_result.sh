#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
S2_TILE=$2

db_filename=${DB##*/}
db_base=${db_filename%.*} 

FOI_INI_DB="${S2_TILE}/FOI_RPG_INI/${db_base}_rpg_ini_all_foi.sqlite"
PREFIX_INI="${db_base}_rpg_ini_all"
PREFIX_INI_LOWER="$(echo $PREFIX_INI | tr '[A-Z]' '[a-z]')"

echo "PREFIX_INI $PREFIX_INI"
echo "PREFIX_INI_LOWER $PREFIX_INI_LOWER"

spatialite $FOI_INI_DB <<END_SQL
SELECT DropGeoTable('${PREFIX_INI}_result');
SELECT DropGeoTable('${PREFIX_INI}_merged');

SELECT DropGeoTable('${PREFIX_INI}_out_foi_ok_clean');
SELECT DropGeoTable('${PREFIX_INI}_out_foi_small_clean');

CREATE TABLE '${PREFIX_INI}_out_foi_ok_clean' (
  id TEXT NOT NULL PRIMARY KEY,
  CODE9 TEXT NOT NULL);
SELECT AddGeometryColumn('${PREFIX_INI}_out_foi_ok_clean', 'GEOMETRY',  (SELECT srid FROM geometry_columns WHERE f_table_name = '${PREFIX_INI_LOWER}') , 'MULTIPOLYGON', 'XY');

INSERT INTO ${PREFIX_INI}_out_foi_ok_clean
SELECT out_foi_ok.id,
       out_foi_ok.CODE9,
    CastToMultiPolygon(ST_Simplify(ST_Intersection(out_foi_ok.GEOMETRY, ST_Buffer(ST_Buffer(out_foi_ok.GEOMETRY, -0.5), 0.7)), 0.5) )
FROM ${PREFIX_INI}_out_foi_ok as out_foi_ok
WHERE ST_IsValid(ST_Buffer(out_foi_ok.GEOMETRY, -0.5));
SELECT CreateSpatialIndex('${PREFIX_INI}_out_foi_ok_clean','GEOMETRY');
CREATE INDEX ${PREFIX_INI}_out_foi_ok_clean_index_id ON ${PREFIX_INI}_out_foi_ok_clean(id);

CREATE TABLE '${PREFIX_INI}_out_foi_small_clean' (
  id TEXT NOT NULL PRIMARY KEY,
  CODE9 TEXT NOT NULL);
SELECT AddGeometryColumn('${PREFIX_INI}_out_foi_small_clean', 'GEOMETRY',  (SELECT srid FROM geometry_columns WHERE f_table_name = '${PREFIX_INI_LOWER}') , 'MULTIPOLYGON', 'XY');

INSERT INTO ${PREFIX_INI}_out_foi_small_clean
SELECT out_foi_small.id,
       out_foi_small.CODE9,
    CastToMultiPolygon(ST_Simplify(ST_Intersection(out_foi_small.GEOMETRY, ST_Buffer(ST_Buffer(out_foi_small.GEOMETRY, -0.5), 0.7)), 0.5) )
FROM ${PREFIX_INI}_out_foi_small as out_foi_small
WHERE ST_IsValid(ST_Buffer(out_foi_small.GEOMETRY, -0.5));
SELECT CreateSpatialIndex('${PREFIX_INI}_out_foi_small_clean','GEOMETRY');
CREATE INDEX ${PREFIX_INI}_out_foi_small_clean_index_id ON ${PREFIX_INI}_out_foi_small_clean(id);

CREATE TABLE '${PREFIX_INI}_result' (
  id TEXT NOT NULL PRIMARY KEY,
  code_cultu TEXT NOT NULL,
  CODE9 TEXT NOT NULL,
  FOI INTEGER,
  foi_type TEXT);
  
SELECT AddGeometryColumn('${PREFIX_INI}_result', 'GEOMETRY', (SELECT srid FROM geometry_columns WHERE f_table_name = '${PREFIX_INI_LOWER}') , 'MULTIPOLYGON', 'XY');

CREATE TABLE '${PREFIX_INI}_merged' (
  id TEXT NOT NULL PRIMARY KEY,
  code_cultu TEXT NOT NULL,
  CODE9 TEXT NOT NULL,
  FOI INTEGER,
  foi_type TEXT);
  
SELECT AddGeometryColumn('${PREFIX_INI}_merged', 'GEOMETRY', (SELECT srid FROM geometry_columns WHERE f_table_name = '${PREFIX_INI_LOWER}') , 'MULTIPOLYGON', 'XY');

INSERT INTO ${PREFIX_INI}_merged
SELECT merged.id,
       merged.code_cultu,
       merged.CODE9,
       merged.FOI,
       merged.foi_type,
       merged.GEOMETRY
FROM
(
	SELECT ini.id, ini.code9 as CODE9, 0 as FOI, 'not_merged' as foi_type, ini.code_cultu, ini.GEOMETRY
	from ${PREFIX_INI_LOWER} as ini, ${PREFIX_INI}_out_foi_ok_dilat as ok_mask
	WHERE ini.ROWID IN (SELECT ROWID FROM SpatialIndex 
						   WHERE f_table_name = '${PREFIX_INI_LOWER}'
						   AND search_frame = ok_mask.GEOMETRY)
		 AND ST_Within(ini.GEOMETRY, ok_mask.GEOMETRY)
	
	UNION
	
	SELECT ini.id, ini.code9 as CODE9, 0 as FOI, 'not_merged' as foi_type, ini.code_cultu, ini.GEOMETRY
	from ${PREFIX_INI_LOWER} as ini, ${PREFIX_INI}_out_foi_small_dilat as small_mask
	WHERE ini.ROWID IN (SELECT ROWID FROM SpatialIndex 
						   WHERE f_table_name = '${PREFIX_INI_LOWER}'
						   AND search_frame = small_mask.GEOMETRY)
		 AND ST_Within(ini.GEOMETRY, small_mask.GEOMETRY)
) as merged;

SELECT CreateSpatialIndex('${PREFIX_INI}_merged', 'GEOMETRY');
CREATE INDEX ${PREFIX_INI}_merged_index_id ON ${PREFIX_INI}_merged(id);

INSERT INTO ${PREFIX_INI}_result
SELECT result.id,
       result.code_cultu,
       result.CODE9,
       result.FOI,
       result.foi_type,
       result.GEOMETRY
FROM
(
	SELECT id, code9 as CODE9, 1 as FOI, 'merged_ok' as foi_type, 'FOI' as code_cultu,
		   CastToMultiPolygon(ST_Union(ST_Buffer(ST_Buffer(foi_ok.GEOMETRY, 2), -2.2), GEOMETRY)) AS GEOMETRY
	FROM ${PREFIX_INI}_out_foi_ok_clean as foi_ok

	union

	SELECT id, code9 as CODE9, 1 as FOI, 'merged_small' as foi_type, 'FOI' as code_cultu,
		   CastToMultiPolygon(ST_Union(ST_Buffer(ST_Buffer(foi_small.GEOMETRY, 2), -2.2), GEOMETRY)) AS GEOMETRY
	FROM ${PREFIX_INI_LOWER}_out_foi_small_clean as foi_small

	union

	SELECT id, code9 as CODE9, 0 as FOI, 'small_not_merged' as foi_type, code_cultu, GEOMETRY
	FROM ${PREFIX_INI_LOWER}_small_no_merge

	union

	SELECT ini.id, ini.code9 as CODE9, 0 as FOI, 'not_merged' as foi_type, ini.code_cultu, ini.GEOMETRY
	FROM ${PREFIX_INI_LOWER} as ini LEFT JOIN 
		( SELECT id FROM ${PREFIX_INI}_merged
		   UNION 
		   SELECT id FROM ${PREFIX_INI_LOWER}_small_no_merge) as exclude 
		 ON ini.id = exclude.id
	WHERE exclude.id IS null 

) AS result;

SELECT CreateSpatialIndex('${PREFIX_INI}_result', 'GEOMETRY');
CREATE INDEX ${PREFIX_INI}_result_index_id ON ${PREFIX_INI}_result(id);

END_SQL

QUERY_FOI="SELECT * FROM ${PREFIX_INI}_result WHERE ST_IsValid(GEOMETRY) == 1"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${PREFIX_INI}_foi_result.shp ${FOI_INI_DB} -dialect sqlite -sql "$QUERY_FOI"
