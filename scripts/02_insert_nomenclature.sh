#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
CODE_CULTU=`realpath ./NOMENCLATURE/code_cultu_2019.csv`
LUT_CULTU_17=`realpath ./NOMENCLATURE/cultu2n17.csv`
LUT_CULTU_43=`realpath ./NOMENCLATURE/cultu2n43.csv`
LUT_CULTU_73=`realpath ./NOMENCLATURE/cultu2n73.csv`
N17=`realpath ./NOMENCLATURE/nomenclature_17c_2018.csv`
N43=`realpath ./NOMENCLATURE/nomenclature_43c_2018.csv`
N73=`realpath ./NOMENCLATURE/nomenclature_73c_2019.csv`

echo "nomenclature file code cultu $CODE_CULTU"

sqlite3 $DB <<END_SQL
DROP TABLE IF EXISTS codes_cultu_ini;
DROP TABLE IF EXISTS codes_cultu;
.separator ";"
.mode csv
.separator ";"
.import '| tail -n +10 $CODE_CULTU' codes_cultu
.import '| tail -n +12 $N17' nomenclature_17
.import '| tail -n +12 $N43' nomenclature_43
.import '| tail -n +13 $N73' nomenclature_73
.import '| tail -n +11 $LUT_CULTU_17' lut_cultu_17
.import '| tail -n +11 $LUT_CULTU_43' lut_cultu_43
.import '| tail -n +11 $LUT_CULTU_73' lut_cultu_73
END_SQL

