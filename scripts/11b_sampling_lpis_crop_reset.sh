#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"

sqlite3 $DB <<END_SQL

UPDATE rpg_ini SET classif_rf = null WHERE classif_rf == 'TRAIN';
UPDATE rpg_ini SET classif_sen4cap = null WHERE classif_sen4cap == 'TRAIN';

UPDATE lpis_crop SET classif_rf = null WHERE classif_rf == 'TRAIN';
UPDATE lpis_crop SET classif_sen4cap = null WHERE classif_sen4cap == 'TRAIN';

UPDATE rpg_ini SET kfold5 = null WHERE kfold5 <> 'VALIDATION';
UPDATE lpis_crop SET kfold5 = null WHERE kfold5 <> 'VALIDATION';

END_SQL
