#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
NOMENCLATURE=`realpath ./NOMENCLATURE/Nomenclature_code_ASP_MAA_2020_V1.csv`
echo "nomenclature file $NOMENCLATURE"

sqlite3 $DB <<END_SQL
DROP TABLE IF EXISTS codes_cultu_ini;
DROP TABLE IF EXISTS codes_cultu;
.separator ";"
.mode csv
.separator ";"
.import '| tail -n +5 $NOMENCLATURE' codes_cultu_ini
ALTER TABLE codes_cultu_ini ADD SURF_2015 REAL;
UPDATE codes_cultu_ini SET SURF_2015 = (SELECT CAST( REPLACE(REPLACE(TRIM([2015]),' ','' ),',','.') as decimal) FROM codes_cultu_ini);
ALTER TABLE codes_cultu_ini ADD SURF_2016 REAL;
UPDATE codes_cultu_ini SET SURF_2016 = (SELECT CAST( REPLACE(REPLACE(TRIM([2016]),' ','' ),',','.') as decimal) FROM codes_cultu_ini);
ALTER TABLE codes_cultu_ini ADD SURF_2017 REAL;
UPDATE codes_cultu_ini SET SURF_2017 = (SELECT CAST( REPLACE(REPLACE(TRIM([2017]),' ','' ),',','.') as decimal) FROM codes_cultu_ini);
ALTER TABLE codes_cultu_ini ADD SURF_2018 REAL;
UPDATE codes_cultu_ini SET SURF_2018 = (SELECT CAST( REPLACE(REPLACE(TRIM([2018]),' ','' ),',','.') as decimal) FROM codes_cultu_ini);
CREATE TABLE codes_cultu AS SELECT Code_culture,Libelle,CODE9EXP,CODE3EXP,SURF_2015,SURF_2016,SURF_2017,SURF_2018,Proposition_Alain_terrain,Proposition_terrain_ajustee,Remarques FROM codes_cultu_ini;
DROP TABLE IF EXISTS codes_cultu_ini;
END_SQL

