#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

TILE=$1

mkdir -p ${TILE}
QUERY_PARCEL="SELECT * FROM rpg_ini WHERE ST_Within(rpg_ini.GEOMETRY, (SELECT GEOMETRY FROM s2_tiles WHERE name = '$TILE'))"
ogr2ogr -dsco SPATIALITE=YES -f "SQLITE" ${TILE}/prep_rpg_2019_$TILE.sqlite prep_rpg_2019.sqlite -nlt MULTIPOLYGON -nln rpg_ini -lco SPATIAL_INDEX=YES -dialect sqlite -sql "$QUERY_PARCEL"

QUERY_VG="SELECT * FROM sna_vg WHERE ST_Within(sna_vg.GEOMETRY, (SELECT GEOMETRY FROM s2_tiles WHERE name = '$TILE')) AND sna_vg.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'sna_vg' AND search_frame = (SELECT GEOMETRY FROM s2_tiles WHERE name = '$TILE'))"
ogr2ogr -update ${TILE}/prep_rpg_2019_$TILE.sqlite prep_rpg_2019.sqlite -nlt PROMOTE_TO_MULTI -nlt MULTIPOLYGON -nln vg_ini -lco SPATIAL_INDEX=YES -dialect sqlite -sql "$QUERY_VG"
QUERY_AT="SELECT * FROM sna_at WHERE ST_Within(sna_at.GEOMETRY, (SELECT GEOMETRY FROM s2_tiles WHERE name = '$TILE')) AND sna_at.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'sna_at' AND search_frame = (SELECT GEOMETRY FROM s2_tiles WHERE name = '$TILE'))"
ogr2ogr -update ${TILE}/prep_rpg_2019_$TILE.sqlite prep_rpg_2019.sqlite -nlt PROMOTE_TO_MULTI -nlt MULTIPOLYGON -nln at_ini -lco SPATIAL_INDEX=YES -dialect sqlite -sql "$QUERY_AT"
QUERY_EA="SELECT * FROM sna_ea WHERE ST_Within(sna_ea.GEOMETRY, (SELECT GEOMETRY FROM s2_tiles WHERE name = '$TILE')) AND sna_ea.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'sna_ea' AND search_frame = (SELECT GEOMETRY FROM s2_tiles WHERE name = '$TILE'))"
ogr2ogr -update ${TILE}/prep_rpg_2019_$TILE.sqlite prep_rpg_2019.sqlite -nlt PROMOTE_TO_MULTI -nlt MULTIPOLYGON -nln ea_ini -lco SPATIAL_INDEX=YES -dialect sqlite -sql "$QUERY_EA"

QUERY_V1="SELECT * FROM sna_v1 WHERE ST_Within(sna_v1.GEOMETRY, (SELECT GEOMETRY FROM s2_tiles WHERE name = '$TILE')) AND sna_v1.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'sna_v1' AND search_frame = (SELECT GEOMETRY FROM s2_tiles WHERE name = '$TILE'))"
ogr2ogr -update ${TILE}/prep_rpg_2019_$TILE.sqlite prep_rpg_2019.sqlite -nlt PROMOTE_TO_MULTI -nlt MULTIPOINT -nln v1_ini -lco SPATIAL_INDEX=YES -dialect sqlite -sql "$QUERY_V1"
