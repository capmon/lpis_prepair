#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
db_filename=${DB##*/}
db_base=${db_filename%.*} 
S2_TILE=$2
echo "S2_TILE ${S2_TILE}"


CROP_FOI_FIELD="id_s, code_cultu, FOI, foi_type, code9 AS CODE9, CODE9_EN, N2019,"
CROP_FOI_FIELD=${CROP_FOI_FIELD}"npix10b5 as npx10b5, npix20b10 as npx20b10, compactness as cmptcness, qa_ratio, classif_rf as rf_t, classif_sen4cap as sen4cap_t, kfold5 as kflod_t,"
CROP_FOI_FIELD=${CROP_FOI_FIELD}" GEOMETRY"

QUERY_CROP_FOI_ALL="SELECT ${CROP_FOI_FIELD} FROM lpis_crop_foi_result WHERE ST_IsValid(GEOMETRY) == 1"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${db_base}_crop_foi_all.shp ${DB} -dialect sqlite -sql "$QUERY_CROP_FOI_ALL"
QUERY_CROP_FOI_VAL="SELECT ${CROP_FOI_FIELD} FROM lpis_crop_foi_result WHERE classif_rf = 'VALIDATION' AND ST_IsValid(GEOMETRY) == 1"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${db_base}_crop_foi_val.shp ${DB} -dialect sqlite -sql "$QUERY_CROP_FOI_VAL"

RPG_FOI_FIELD="id, code_cultu, FOI, foi_type, code9 AS CODE9, CODE9_EN, N2019,"
RPG_FOI_FIELD=${RPG_FOI_FIELD}"npix10b5 as npx10b5, npix20b10 as npx20b10, compactness as cmptcness, qa_ratio, classif_rf as rf_t, classif_sen4cap as sen4cap_t, kfold5 as kflod_t,"
RPG_FOI_FIELD=${RPG_FOI_FIELD}" GEOMETRY"

QUERY_RPG_FOI_ALL="SELECT ${RPG_FOI_FIELD} FROM rpg_foi_result WHERE ST_IsValid(GEOMETRY) == 1"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${db_base}_rpg_foi_all.shp ${DB} -dialect sqlite -sql "$QUERY_RPG_FOI_ALL"
QUERY_RPG_FOI_VAL="SELECT ${RPG_FOI_FIELD} FROM rpg_foi_result WHERE classif_rf = 'VALIDATION' AND ST_IsValid(GEOMETRY) == 1"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${db_base}_rpg_foi_val.shp ${DB} -dialect sqlite -sql "$QUERY_RPG_FOI_ALL"
