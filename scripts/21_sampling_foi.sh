#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
CODE_DIR=$2
echo "dir to rpg/lpis preprocess code = ${CODE_DIR}"

sqlite3 $DB <<END_SQL

UPDATE rpg_foi_result SET classif_rf = null WHERE classif_rf == 'TRAIN';
UPDATE rpg_foi_result SET classif_sen4cap = null WHERE classif_sen4cap == 'TRAIN';
UPDATE rpg_foi_result SET kfold5 = null WHERE kfold5 <> 'VALIDATION';

UPDATE lpis_crop_foi_result SET classif_rf = null WHERE classif_rf == 'TRAIN';
UPDATE lpis_crop_foi_result SET classif_sen4cap = null WHERE classif_sen4cap == 'TRAIN';
UPDATE lpis_crop_foi_result SET kfold5 = null WHERE kfold5 <> 'VALIDATION';

END_SQL


WHERE_CLAUSE="WHERE npix10b5 >= 10 AND (classif_rf <> 'VALIDATION' OR classif_rf IS null)"
python ${CODE_DIR}/script/rpg_sample_sql.py --in_db ${DB} --in_table rpg_foi_result --id_field id --code_field CODE9 --min_size 30 --strategy rf_2018 --classif_field classif_rf --where_clause "${WHERE_CLAUSE}"
WHERE_CLAUSE="WHERE npix10b5 >= 10 AND (classif_sen4cap <> 'VALIDATION' OR classif_sen4cap IS null)"
python ${CODE_DIR}/script/rpg_sample_sql.py --in_db ${DB} --in_table rpg_foi_result --id_field id --code_field CODE9 --min_size 30 --strategy sen4cap --classif_field classif_sen4cap --where_clause "${WHERE_CLAUSE}"
WHERE_CLAUSE="WHERE npix10b5 >= 10 AND (kfold5 <> 'VALIDATION' OR kfold5 IS null)"
python ${CODE_DIR}/script/rpg_sample_sql.py --in_db ${DB} --in_table rpg_foi_result --id_field id --code_field CODE9 --min_size 30 --strategy kfold --num_fold 5 --classif_field kfold5 --where_clause "${WHERE_CLAUSE}"

WHERE_CLAUSE="WHERE npix10b5 >= 10 AND (classif_rf <> 'VALIDATION' OR classif_rf IS null)"
python ${CODE_DIR}/script/rpg_sample_sql.py --in_db ${DB} --in_table lpis_crop_foi_result --id_field id_s --code_field CODE9  --min_size 30 --strategy rf_2018 --classif_field classif_rf --where_clause "${WHERE_CLAUSE}"
WHERE_CLAUSE="WHERE npix10b5 >= 10 AND (classif_sen4cap <> 'VALIDATION' OR classif_sen4cap IS null)"
python ${CODE_DIR}/script/rpg_sample_sql.py --in_db ${DB} --in_table lpis_crop_foi_result --id_field id_s --code_field CODE9 --min_size 30 --strategy sen4cap --classif_field classif_sen4cap --where_clause "${WHERE_CLAUSE}"
WHERE_CLAUSE="WHERE npix10b5 >= 10 AND (kfold5 <> 'VALIDATION' OR kfold5 IS null)"
python ${CODE_DIR}/script/rpg_sample_sql.py --in_db ${DB} --in_table lpis_crop_foi_result --id_field id_s --code_field CODE9 --min_size 30 --strategy kfold --num_fold 5 --classif_field kfold5 --where_clause "${WHERE_CLAUSE}"

        
