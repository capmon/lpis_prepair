#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
S2_TILE=$2

db_filename=${DB##*/}
db_base=${db_filename%.*} 

FOI_INI_DB="${S2_TILE}/FOI_RPG_INI/${db_base}_rpg_ini_all_foi.sqlite"
PREFIX_RPG_INI="${db_base}_rpg_ini_all"
PREFIX_RPG_INI_LOWER="$(echo $PREFIX_INI | tr '[A-Z]' '[a-z]')"

echo "PREFIX_RPG_INI $PREFIX_RPG_INI"
echo "PREFIX_RPG_INI_LOWER $PREFIX_RPG_INI_LOWER"

QUERY_MULTI="SELECT * FROM ${PREFIX_RPG_INI}_result WHERE ST_IsValid(GEOMETRY) AND ST_NumGeometries(GEOMETRY) > 1"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${PREFIX_RPG_INI}_foi_result_multi.shp ${FOI_INI_DB} -dialect sqlite -sql "$QUERY_MULTI"

FOI_CROP_DB="${S2_TILE}/FOI_LPIS_CROP/${db_base}_lpis_crop_all_foi.sqlite"
PREFIX_CROP_INI="${db_base}_lpis_crop_all"
PREFIX_CROP_INI_LOWER="$(echo $PREFIX_INI | tr '[A-Z]' '[a-z]')"
echo "PREFIX_CROP_INI $PREFIX_CROP_INI"
echo "PREFIX_CROP_INI_LOWER $PREFIX_CROP_INI_LOWER"

QUERY_MULTI="SELECT * FROM ${PREFIX_CROP_INI}_result WHERE ST_IsValid(GEOMETRY) AND ST_NumGeometries(GEOMETRY) > 1"
ogr2ogr -f "ESRI Shapefile" ${S2_TILE}/${PREFIX_CROP_INI}_foi_result_multi.shp ${FOI_CROP_DB} -dialect sqlite -sql "$QUERY_MULTI"

QUERY_MULTI="SELECT * FROM rpg_ini WHERE ST_IsValid(GEOMETRY) AND ST_NumGeometries(ST_Buffer(GEOMETRY, -1)) > 1"
