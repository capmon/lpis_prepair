#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"

sqlite3 $DB <<END_SQL
ALTER TABLE rpg_foi_result ADD CODE9_EN VARCHAR;
ALTER TABLE rpg_foi_result ADD N2019 INTEGER;
UPDATE rpg_foi_result 
SET (N2019, CODE9_EN) = (
  SELECT CAST(cultu_73.N2020 as integer), cultu_73.CODE9_EN
  FROM (
     SELECT CODE9, N2020, CODE9_EN 
       FROM nomenclature_73
     ) AS cultu_73 
  WHERE rpg_foi_result.CODE9 = cultu_73.CODE9);
  
ALTER TABLE lpis_crop_foi_result ADD CODE9_EN VARCHAR;
ALTER TABLE lpis_crop_foi_result ADD N2019 INTEGER;
UPDATE lpis_crop_foi_result 
SET (N2019, CODE9_EN) = (
  SELECT CAST(cultu_73.N2020 as integer), cultu_73.CODE9_EN
  FROM (
     SELECT CODE9, N2020, CODE9_EN 
       FROM nomenclature_73
     ) AS cultu_73 
  WHERE lpis_crop_foi_result.CODE9 = cultu_73.CODE9);
END_SQL
