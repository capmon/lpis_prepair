#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
S2_TILE=$2
echo "S2_TILE ${S2_TILE}"
GROUNDTRUTH=`realpath ./GROUNDTRUTH/CSP_code_pacage_2019_within_T${S2_TILE}.csv`

sqlite3 $DB <<END_SQL
DROP TABLE IF EXISTS validation_id;
.mode csv
.separator ","
CREATE TABLE validation_id (
  pacage TEXT NOT NULL PRIMARY KEY);
.import $GROUNDTRUTH validation_id
UPDATE validation_id SET pacage = substr('000000000'||pacage, -9, 9);
CREATE INDEX validation_id_index_pacage ON validation_id(pacage);

UPDATE rpg_ini
SET classif_rf = "VALIDATION" 
WHERE pacage IN ( SELECT pacage FROM validation_id WHERE pacage = rpg_ini.pacage);

UPDATE lpis_crop
SET classif_rf = "VALIDATION" 
WHERE pacage IN ( SELECT pacage FROM validation_id WHERE pacage = lpis_crop.pacage);

UPDATE rpg_ini
SET classif_sen4cap = "VALIDATION" 
WHERE pacage IN ( SELECT pacage FROM validation_id WHERE pacage = rpg_ini.pacage);

UPDATE rpg_ini
SET kfold5 = "VALIDATION" 
WHERE pacage IN ( SELECT pacage FROM validation_id WHERE pacage = rpg_ini.pacage);

UPDATE lpis_crop
SET classif_sen4cap = "VALIDATION" 
WHERE pacage IN ( SELECT pacage FROM validation_id WHERE pacage = lpis_crop.pacage);

UPDATE lpis_crop
SET kfold5 = "VALIDATION" 
WHERE pacage IN ( SELECT pacage FROM validation_id WHERE pacage = lpis_crop.pacage);

END_SQL
