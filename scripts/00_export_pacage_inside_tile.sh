#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"

S2_TILE=$2
echo "S2_TILE $S2_TILE"

spatialite $DB <<END_SQL

.headers on
.mode csv
.output  T${S2_TILE}_pacage_in.csv

SELECT pacage
FROM 
  ( SELECT rpg_ini.pacage, COUNT(*) as count_all, parcel_in_tile.count_tile
    FROM rpg_ini
    INNER JOIN (
        SELECT COUNT(*) as count_tile, pacage
        FROM rpg_ini 
        WHERE ST_Within(rpg_ini.GEOMETRY, (SELECT GEOMETRY 
                                        FROM s2_tiles 
                                        WHERE name = '${S2_TILE}'))
              AND rpg_ini.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'rpg_ini' AND search_frame = (SELECT GEOMETRY FROM s2_tiles WHERE name = '${S2_TILE}'))
        GROUP BY pacage
        ) AS parcel_in_tile
      ON rpg_ini.pacage = parcel_in_tile.pacage
    GROUP BY rpg_ini.pacage
  ) AS pacage_count
WHERE count_all = count_tile;

.output T${S2_TILE}_pacage_out.csv
SELECT pacage
FROM 
  ( SELECT rpg_ini.pacage, COUNT(*) as count_all, parcel_in_tile.count_tile
    FROM rpg_ini
    INNER JOIN (
        SELECT COUNT(*) as count_tile, pacage
        FROM rpg_ini 
        WHERE ST_Within(rpg_ini.GEOMETRY, (SELECT GEOMETRY 
                                        FROM s2_tiles 
                                        WHERE name = '${S2_TILE}'))
              AND rpg_ini.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'rpg_ini' AND search_frame = (SELECT GEOMETRY FROM s2_tiles WHERE name = '${S2_TILE}'))
        GROUP BY pacage
        ) AS parcel_in_tile
      ON rpg_ini.pacage = parcel_in_tile.pacage
    GROUP BY rpg_ini.pacage
  ) AS pacage_count
WHERE count_all != count_tile;

.output T${S2_TILE}_pacage_all.csv

SELECT pacage
FROM rpg_ini 
WHERE ST_Within(rpg_ini.GEOMETRY, (SELECT GEOMETRY 
                                        FROM s2_tiles 
                                        WHERE name = '${S2_TILE}'))
     AND rpg_ini.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'rpg_ini' AND search_frame = (SELECT GEOMETRY FROM s2_tiles WHERE name = '${S2_TILE}'))              AND rpg_ini.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'rpg_ini' AND search_frame = (SELECT GEOMETRY FROM s2_tiles WHERE name = '${S2_TILE}'))
GROUP BY pacage;

.mode list
.output stdout

SELECT COUNT(*)
FROM 
  ( SELECT rpg_ini.pacage, COUNT(*) as count_all, parcel_in_tile.count_tile
    FROM rpg_ini
    INNER JOIN (
        SELECT COUNT(*) as count_tile, pacage
        FROM rpg_ini 
        WHERE ST_Within(rpg_ini.GEOMETRY, (SELECT GEOMETRY 
                                        FROM s2_tiles 
                                        WHERE name = '${S2_TILE}'))
           AND rpg_ini.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'rpg_ini' AND search_frame = ST_Buffer((SELECT GEOMETRY FROM s2_tiles WHERE name = '${S2_TILE}'), 1000))
        GROUP BY pacage
        ) AS parcel_in_tile
      ON rpg_ini.pacage = parcel_in_tile.pacage
    GROUP BY rpg_ini.pacage
  ) AS pacage_count
WHERE count_all = count_tile;

SELECT COUNT(*)
FROM 
  ( SELECT rpg_ini.pacage, COUNT(*) as count_all, parcel_in_tile.count_tile
    FROM rpg_ini
    INNER JOIN (
        SELECT COUNT(*) as count_tile, pacage
        FROM rpg_ini 
        WHERE ST_Within(rpg_ini.GEOMETRY, (SELECT GEOMETRY 
                                        FROM s2_tiles 
                                        WHERE name = '${S2_TILE}'))
              AND rpg_ini.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'rpg_ini' AND search_frame = ST_Buffer((SELECT GEOMETRY FROM s2_tiles WHERE name = '${S2_TILE}'), 1000))
        GROUP BY pacage
        ) AS parcel_in_tile
      ON rpg_ini.pacage = parcel_in_tile.pacage
    GROUP BY rpg_ini.pacage
  ) AS pacage_count
WHERE count_all != count_tile;

SELECT COUNT(*)
FROM (
	SELECT pacage
	FROM rpg_ini 
	WHERE ST_Within(rpg_ini.GEOMETRY, (SELECT GEOMETRY 
											FROM s2_tiles 
											WHERE name = '${S2_TILE}'))
		 AND rpg_ini.ROWID IN( SELECT ROWID FROM SpatialIndex WHERE f_table_name = 'rpg_ini' AND search_frame = St_Buffer((SELECT GEOMETRY FROM s2_tiles WHERE name = '${S2_TILE}'),1000))
	GROUP BY pacage 
);

END_SQL
