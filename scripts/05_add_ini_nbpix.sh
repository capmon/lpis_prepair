#!/bin/bash

##
## Copyright (c) 2020 IGN France.
##
## This file is part of lpis_prepair
## (see https://gitlab.com/capmon/lpis_prepair).
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
##
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

DB=$1
echo "sqlite db $DB"
S2_TILE=$2
echo "S2_TILE $S2_TILE"
CODE_DIR=$3
echo "dir to rpg/lpis preprocess code = ${CODE_DIR}"

python ${CODE_DIR}/script/rpg_add_nbpixel_sql.py --in_db ${DB} --in_table rpg_ini --s2_tile ${S2_TILE} --out_field npix10b5 --buffer_size 5 --resolution 10 --num_max 50000 --out_mask lpis_${S2_TILE}_npix10b5_rpg_ini.tif
python ${CODE_DIR}/script/rpg_add_nbpixel_sql.py --in_db ${DB} --in_table rpg_ini --s2_tile ${S2_TILE} --out_field npix20b10 --buffer_size 10 --resolution 20 --num_max 50000 --out_mask lpis_${S2_TILE}_npix20b10_rpg_ini.tif

